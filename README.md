# My solutions to advent of code challenges

## How to use

### SQL

Spawn a (recent) postgresql database.

Connect to it, and for each day, load data with `load.sql`, execute each part with `part1.sql` and `part2.sql`.

### Rust

To init a new year:

- create `year_<year>/mod.rs`
- add it to `lib.rs`
- add an entry in the BTreeMap years_fn in `main.rs`

To init a new day, copy `./template.rs` into `year_<year>/<day>.rs`, and edit the corresponding `mod.rs`. Save the day input into `year_<year>/inputs/<day>` as well.

To run the tests (based on example input): `cargo test`.

To run the real solutions, `cargo run -- --help`.

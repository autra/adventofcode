with recursive
subs as (
  select
    pos::int
  from
    day7,
    regexp_split_to_table(line, ',') pos
),

-- brute force actually work here :-)
-- but it takes ~ 400ms in my machine.
--  select
  --  rendezvous, fuel.consumption
--  from
  --  generate_series((select min(pos) from subs), (select max(pos) from subs)) rendezvous,
  --  lateral (select sum((pos^2 + rendezvous^2 - 2*rendezvous*pos + abs(pos - rendezvous)) / 2) consumption from subs) as fuel
--  order by rendezvous
--
-- but I prefer to be more clever:
-- part1 proves us there is one minimum for fuel consumption.
-- I don't have formal proof for that, but the new calculus also has a minimum, and I think it'll be actually nearer the mean.
-- so let's calculate the mean, and interate until we find a mean.
-- this takes ~ 10ms in my machine
iteration as (
  select
    1 as it,
    rendezvous.pos,
    fuel.consumption
  from
    (select round(avg(pos)) pos from subs) as rendezvous,
    lateral (
      select
        sum(
          (subs.pos^2 + rendezvous.pos^2 - 2*rendezvous.pos*subs.pos + abs(subs.pos - rendezvous.pos)) / 2
        ) consumption
      from subs
    ) as fuel

  union all

  select
    it+1 as it, min.pos, min.consumption
  from
    iteration,
    lateral (
      with
      rendezvous as (
        select
          iteration.pos + d as pos
        from
          generate_series(-1, 1, 2) d
      ),
      conso as (
        select
          rendezvous.pos,
          sum(
            (subs.pos^2 + rendezvous.pos^2 - 2*rendezvous.pos*subs.pos + abs(subs.pos - rendezvous.pos)) / 2
          ) consumption
        from
          rendezvous,
          subs
        group by rendezvous.pos
      )
      select consumption, pos
      from conso
      where consumption=(select min(consumption) from conso)
    ) min
    where min.consumption < iteration.consumption
)
select * from iteration
order by it desc
limit 1
;

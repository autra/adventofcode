
drop table if exists day7;

create table day7(line_number integer not null generated always as identity, line text);

\copy day7(line) from './input'

vacuum analyze day7;

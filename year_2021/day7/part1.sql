with subs as (
  select
    pos::int
  from
    day7,
    regexp_split_to_table(line, ',') pos
)
-- suppose we choose the position k
-- 0 -x---x--------x--k----x--x-x-x-x-----x----> N
-- if we move k to the right of 1 units, the 3 points to the left would
-- increase the sum of distance of 1 and the 6 point to the right would
-- decrease it of 6. Therefore, to minimize the distance, we need to have the
-- same number of points on each side of k, which is actually the median.
-- In postgresql, we calculate it with percentile_cont
select
  sum(abs(subs.pos - optimal.pos)) as "Answer!!"
from
  subs,
  (
    select
      percentile_cont(0.5) within group (order by pos) as pos
    from
      subs
  ) as optimal(pos)
;

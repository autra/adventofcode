with recursive iteration as (
  -- initial term, just the input exploded with coordinates
  select
    0 as it,
    x,
    line_number as y,
    octopus::int,
    0 as flash
  from
    day11,
    regexp_split_to_table(line, '') with ordinality as _(octopus, x)
  union
  -- recursive term, aka one step:
  (
    -- but each step calculation is itself iterative, let's call it propagation
    with recursive current_it as (table iteration),
    propagation as (
      -- current iteration state
      select
        it+1 as it,
        1 as it2,
        x,
        y,
        octopus + 1 as octopus,
        flash
      from
        current_it
      where
	-- this is the stop condition: when every octopus is 0
        exists(select from current_it where octopus != 0)

      union
      (
        -- trick to be able to use the recursive term several time
        with current as ( table propagation)
        -- actual propagation calculation
        select
          it,
          it2 + 1 as it2,
          x,
          y,
          (case
            -- note: an octopus can only flash once per iteration
            when octopus > 9 or octopus = 0 then
              0
            else
              octopus
                + (greatest(sign(lag(octopus) over horizontal - 9), 0))
                + (greatest(sign(lead(octopus) over horizontal - 9), 0))
                + (greatest(sign(lag(octopus) over vertical - 9), 0))
                + (greatest(sign(lead(octopus) over vertical - 9), 0))
                + (greatest(sign(lag(octopus) over diag1 - 9), 0))
                + (greatest(sign(lead(octopus) over diag1 - 9), 0))
                + (greatest(sign(lag(octopus) over diag2 - 9), 0))
                + (greatest(sign(lead(octopus) over diag2 - 9), 0))
          end)::int,
          -- keep how many time this particular octopus has flashed
          case when octopus > 9 then
            flash + 1
          else
            flash
          end
        from
          current
        where
          -- we keep doing the propagation til we have octopuses > 10
          exists (select from current where octopus > 9)
          -- keep only the last propagation iteration
          -- not sure if I really need this, but this condition makes everything ~ 8 times faster
          and current.it2=(select max(it2) from current)
        window
          horizontal as (partition by y order by x),
          vertical as (partition by x order by y),
          diag1 as (partition by x-y order by x+y),
          diag2 as (partition by x+y order by x-y)
      )
    )
    select it, x, y, octopus, flash from propagation
    where
      it2=(select max(it2) from propagation)
  )
)
select max(it) as "Answer!!" from iteration;

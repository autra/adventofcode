
drop table if exists day11;

create table day11(line_number integer not null generated always as identity, line text);

\copy day11(line) from './input'

vacuum analyze day11;

explain analyze with recursive
edges as materialized (
  select
    split_part(line, '-', 1) as left,
    split_part(line, '-', 2) as right,
    lower(split_part(line, '-', 2))=split_part(line, '-', 2) as right_is_lower

  from day12

  union

  -- this is a perf optim, it's (slightly) better to have an oriented graph,
  -- because the join condition in the recursive term are simpler, and
  -- postgresql is then able to use a hash join instead of a nested loop
  -- the gain is ~ 1sec over 6, so it's not like it's changing the world but well...
  select
    split_part(line, '-', 2) as left,
    split_part(line, '-', 1) as right,
    lower(split_part(line, '-', 1))=split_part(line, '-', 1) as right_is_lower
  from day12
),
walk as (
  select
    'start' as node,
    array['start'] as visited,
    false as lower_case_visited_2_times

  union all

  select
    edges.right,
    visited || edges.right,
    lower_case_visited_2_times or (edges.right_is_lower and edges.right = any(visited))
  from
    walk
    join edges on walk.node=edges.left
    where
      not edges.right='start'
      and not walk.node='end'
      and (
        edges.right = 'end'
        or not edges.right_is_lower
        or not (edges.right = any(visited)) or not lower_case_visited_2_times
      )
)
select count(*) as "Answer!!"
from walk
where node='end';


drop table if exists day12;

create table day12(line_number integer not null generated always as identity, line text);

\copy day12(line) from './input2'

vacuum analyze day12;

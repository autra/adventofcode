
drop table if exists day12;

create table day12(line_number integer not null generated always as identity, line text);

\copy day12(line) from './input'

vacuum analyze day12;

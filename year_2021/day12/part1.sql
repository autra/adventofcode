with recursive
edges as (
  select
    split_part(line, '-', 1) as left,
    split_part(line, '-', 2) as right
  from day12
),
walk as (
  select
    'start' as node,
    array['start'] as visited

  union all

  select
    new.node,
    visited || new.node
  from
    walk
    join edges on
      (walk.node=edges.left and not lower(edges.right) = any(visited))
      or (walk.node=edges.right and not lower(edges.left) = any(visited)),
      lateral(
        select case when walk.node = edges.left then edges.right else edges.left end as node
      ) new
    where not walk.node='end'
  )
  select count(*) as "Answer!!"
  from walk
  where node='end';

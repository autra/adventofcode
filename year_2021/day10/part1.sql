with
matching_pairs(open, close) as (
  values ('(', ')'), ('[', ']'), ('<', '>'), ('{', '}')
), costs(char, cost) as (
  values (')', 3), (']', 57), ('}', 1197), ('>', 25137)
)
select
  sum(costs.cost) as "Answer!!"
from
  day10,
  -- doing in lateral for performance. Lines are independant from each other,
  -- so treating them together would make the levels x levels join too costly
  lateral (
    -- assign a nesting "level" to each char
    with levels as (
      select
        idx,
        char,
        -- number of opening parenthesis
        count(*) filter (where char in ('(', '[', '{', '<')) over (order by idx)
          -- minus number of closing (excluding this one)
          - count(*) filter (where char in (')', ']', '}', '>'))
            over (
              order by idx
              rows unbounded preceding exclude current row
            )
        as level
      from
        regexp_split_to_table(line, '') with ordinality splitted(char, idx)
    )
    select
      c.cost
    from
      levels closing
      join matching_pairs matching on matching.close = closing.char
      join costs c on c.char=closing.char
      cross join lateral (
        select
          char,
          idx
        from levels opening
        where
          -- the matching char is the immediate previous with same level
          opening.level = closing.level
          and opening.idx < closing.idx
        order by opening.idx desc
        limit 1
      ) opening
    where
      closing.char in (')', ']', '>', '}')
      -- keep only when the closing char is incorrect
      and opening.char != matching.open
  ) costs

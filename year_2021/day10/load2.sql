
drop table if exists day10;

create table day10(line_number integer not null generated always as identity, line text);

\copy day10(line) from './input2'

vacuum analyze day10;

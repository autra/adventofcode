with
matching_pairs(open, close) as (
  values ('(', ')'), ('[', ']'), ('<', '>'), ('{', '}')
), points(char, point) as (
  values (')', 1), (']', 2), ('}', 3), ('>', 4)
),
incomplete as (
  select
    line_number, line, test.incorrect
  from
    day10
    -- doing in lateral for performance. Lines are independant from each other,
    -- so treating them together would make the levels x levels join too costly
    -- this lateral serves to eliminate invalid lines and keep only incomplete
    left join lateral (
      -- assign a nesting "level" to each char
      with levels as (
        select
          idx,
          char,
          -- number of opening parenthesis
          count(*) filter (where char in ('(', '[', '{', '<')) over (order by idx)
            -- minus number of closing (excluding this one)
            - count(*) filter (where char in (')', ']', '}', '>'))
              over (
                order by idx
                rows unbounded preceding exclude current row
              )
          as level
        from
          regexp_split_to_table(line, '') with ordinality splitted(char, idx)
      )
      select
        true as incorrect
      from
        levels closing
        join matching_pairs matching on matching.close = closing.char
        cross join lateral (
          select
            char,
            idx
          from levels opening
          where
            -- the matching char is the immediate previous with same level
            opening.level = closing.level
            and opening.idx < closing.idx
          order by opening.idx desc
          limit 1
        ) opening
      where
        closing.char in (')', ']', '>', '}')
        -- keep only when the closing char is incorrect
        and opening.char != matching.open
    ) test on true
  -- keeping only not incorrect (aka correct and incomplete) values
  where not coalesce(incorrect, false)
)
select
  percentile_disc(0.5) within group(order by calc.score) as "Answer!!"
from
  incomplete,
  lateral (
    -- assign a nesting "level" to each char
    -- I didn't find an nice way to factorize with previous calculation. oh well...
    with levels as (
      select
        idx,
        char,
        -- number of opening parenthesis
        count(*) filter (where char in ('(', '[', '{', '<')) over (order by idx)
          -- minus number of closing (excluding this one)
          - count(*) filter (where char in (')', ']', '}', '>'))
            over (
              order by idx
              rows unbounded preceding exclude current row
            )
        as level
      from
        regexp_split_to_table(line, '') with ordinality splitted(char, idx)
    ),
    -- what is the last level of the non-closing char that needs matching?
    last as (
      select
        case when char in (')', ']', '>', '}') then
          level - 1
        else
          level
        end as level
      from
        levels
      order by idx desc
      limit 1
    ),
    -- find the missing closing chars, keep the order for the calculation
    missing_matching as (
      select
        m.close as closing,
        row_number() over (order by opening_level) as id
      from
        last
        cross join lateral generate_series(last.level, 1, -1) opening_level
        cross join lateral (
          select
            char
          from
            levels opening
          where
            opening.level = opening_level
          order by idx desc
          limit 1
        ) dangling
        join matching_pairs m on m.open=dangling.char
    )
      select
        sum((5^(id-1)) * point) AS score
      from missing_matching
        join points on points.char = missing_matching.closing
  ) calc
;

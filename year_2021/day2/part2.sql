with diff as (
  select
    forward as forward_diff,
    forward * sum(coalesce((regexp_match(line, 'down (\d)'))[1]::int, 0)
                  - coalesce((regexp_match(line, 'up (\d)'))[1]::int, 0))
              over (order by line_number)
      as depth_diff

  from
    day2,
    coalesce((regexp_match(line, 'forward (\d)'))[1]::int, 0) as forward
)
select sum(forward_diff) * sum(depth_diff) as "Answer!!" from diff;

drop table if exists day2;

create table day2(line_number integer not null generated always as identity, line text);

\copy day2(line) from './input2'

vacuum analyze day2;

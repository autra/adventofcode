with displacement as (
  select
    sum(coalesce((regexp_match(line, 'forward (\d)'))[1]::int, 0)) as forward,
    sum(coalesce((regexp_match(line, 'down (\d)'))[1]::int, 0)
        - coalesce((regexp_match(line, 'up (\d)'))[1]::int, 0)) as depth
  from
    day2
) select forward * depth as "Answer!!" from displacement
;

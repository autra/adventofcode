
drop table if exists day4;

create table day4(line_number integer not null generated always as identity, line text);

\copy day4(line) from './input2'

vacuum analyze day4;

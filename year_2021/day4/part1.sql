with recursive
-- parse input
input as (
  select row_number() over () as i, s
    from
      day4,
      regexp_split_to_table(line, ',')
  s where line_number=1
),
-- assign id to each grid's lines
grid_with_id as (
  select
    -- count blank line before the current line to have grid ids
    count(*) filter (where line = '') over (order by line_number) as grid_id,
    line_number,
    trim(line) as line
  from day4
),
-- assign y coordinate to each line
grid_with_y as (
  select grid_id, row_number() over (partition by grid_id order by line_number) as y, line
  from grid_with_id
  where line != '' and grid_id > 0
),
-- assign x coordinate to each cell
grid_xy as (
  select grid_id, row_number() over (partition by grid_id, y order by y) as x, y, c
  from grid_with_y, regexp_split_to_table(line, ' +') c
),
-- do the game
game as (
  select 0 as pt, grid_id, x, y, c, false as marked, false as won
  from grid_xy

  union all

  (
    with current_game as (table game),
    new_marked as (
      select
        pt+1 as pt, grid_id, x, y, c, marked or s=c as marked
      from
        current_game,
        lateral (select i, s from input where i = pt+1) _(input)
      -- stop condition
      where not exists(select from current_game where won)
    )
      select
        pt, grid_id, x, y, c, marked,
        bool_and(marked) over (partition by grid_id, x) or bool_and(marked) over (partition by grid_id, y) as won
      from new_marked
  )
)
-- select 1/ the winning grid 2/ cells in this grid that are not marked
-- and 3/ sum these, join with input to get the last drawn number, and multiply
select sum(c::int) filter (where not marked) * input.s::int as "Answer!!"
from
  game
  join input on i=pt
where
  pt=(select max(pt) from game)
  and grid_id in (select distinct grid_id from game where won)
group by input.s
;

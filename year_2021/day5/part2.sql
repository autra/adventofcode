with geom as (
  select
    line_number,
    m[1]::int as x1,
    m[2]::int as y1,
    m[3]::int as x2,
    m[4]::int as y2
  from
    day5,
    regexp_matches(line, '(\d+),(\d+) -> (\d+),(\d+)') m
),
intersection as (
  select
    -- if x
    x1 + delta * sign(x2 - x1)::int as x,
    -- the sign multiplication is just a way to tell if Xs and Ys have the same slope
    y1 + delta * sign(y2 - y1)::int as y,
    count(*)
  from
    geom,
    lateral (
      select
        case when x1=x2 then sign(y2-y1)::int * (y2 - y1) else sign(x2-x1)::int * (x2 - x1) end as upper
    ) bounds,
    generate_series(0, bounds.upper) delta
  group by x, y
)
select
  count(*) as "Answer!!"
from
  intersection
where count > 1;

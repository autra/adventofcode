
drop table if exists day5;

create table day5(line_number integer not null generated always as identity, line text);

\copy day5(line) from './input2'

vacuum analyze day5;


drop table if exists day5;

create table day5(line_number integer not null generated always as identity, line text);

\copy day5(line) from './input'

vacuum analyze day5;

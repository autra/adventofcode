with geom as (
  select
    m[1]::int as x1,
    m[2]::int as y1,
    m[3]::int as x2,
    m[4]::int as y2
  from
    day5,
    regexp_matches(line, '(\d+),(\d+) -> (\d+),(\d+)') m
),
intersection as (
  select
    x,
    y,
    count(*)
  from
    geom,
    generate_series(least(x1, x2), greatest(x1, x2)) x,
    generate_series(least(y1, y2), greatest(y1, y2)) y
  where
    x1=x2
    or y1=y2
  group by x, y
)
select
  count(*) as "Answer!!"
from
  intersection
where count > 1;

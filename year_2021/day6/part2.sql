with recursive
fish as (
  select
    0 as day,
    ARRAY[
      (count(*) filter (where day::int=0)),
      (count(*) filter (where day::int=1)),
      (count(*) filter (where day::int=2)),
      (count(*) filter (where day::int=3)),
      (count(*) filter (where day::int=4)),
      (count(*) filter (where day::int=5)),
      (count(*) filter (where day::int=6)),
      (count(*) filter (where day::int=7)),
      (count(*) filter (where day::int=8))
    ] as fish_per_days
  from
    day6,
    regexp_split_to_table(line, ',') day

  union all
  select
    day+1 as day,
    array[
      fish_per_days[2],
      fish_per_days[3],
      fish_per_days[4],
      fish_per_days[5],
      fish_per_days[6],
      fish_per_days[7],
      fish_per_days[8] + fish_per_days[1],
      fish_per_days[9],
      fish_per_days[1]
    ]
    from fish
    where day < 256
)
select sum(nb)
from
fish,
unnest(fish_per_days) nb
where day = 256;

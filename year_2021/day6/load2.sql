
drop table if exists day6;

create table day6(line_number integer not null generated always as identity, line text);

\copy day6(line) from './input2'

vacuum analyze day6;

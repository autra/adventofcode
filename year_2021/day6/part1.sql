with recursive
fish as (
select
  day::int
from
  day6,
  regexp_split_to_table(line, ',') day
  --where day::int = 1

  union all

  select
    -- instead of simulating, we iteratively create new fish with a counter as
    -- if they were there from the beginning
    -- a fish with counter at T generate a new fish with counter at 8+T+1 then
    -- every 7 days
    (day + 1 + 8) + (new_fish_day - 1) * 7
  from
    fish,
    -- 1 to 80 / 7, but shifted to account for offset
    generate_series(1, (:total_day + (7 - day - 1)) / 7) new_fish_day
  where day < :total_day
)
select count(*) as "Answer!!" from fish;

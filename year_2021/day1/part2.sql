with rolling_diff as (
  select
    lead(line, 3) over (order by line_number)::int - line::int > 0 as increase
  from day1
)
select
  count(*)
from rolling_diff
where increase
;

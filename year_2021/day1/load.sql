drop table if exists day1;

create table day1(line_number integer not null generated always as identity, line text);

\copy day1(line) from './input'

vacuum analyze day1;

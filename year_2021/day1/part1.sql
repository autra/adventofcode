select
  count(*) filter (where diff > 0)
from (
  select
    line::int - lag(line, 1) over (order by line_number)::int as diff
  from day1
) _;

with map as (
  select
    line_number as y,
    x,
    char::int as height
  from
    day9,
    regexp_split_to_table(line, '') with ordinality as _(char, x)
),
search as (
  select
    x,
    y,
    height + 1 risk_level,
    coalesce(height < lag(height, 1) over y_window, true)
    and coalesce(height < lead(height, 1 ) over y_window, true)
    and coalesce(height < lag(height, 1) over x_window, true)
    and coalesce(height < lead(height, 1 ) over x_window, true)
    as is_min
  from
    map
  window
    y_window as (partition by y order by x),
    x_window as (partition by x order by y)
)
select
  sum(risk_level) as "Answer!!"
from
  search
where is_min
;

with recursive
map as (
  select
    line_number as y,
    x,
    char::int as height
  from
    day9,
    regexp_split_to_table(line, '') with ordinality as _(char, x)
),
low_points as (
  select
    x,
    y,
    height,
    coalesce(height < lag(height, 1) over y_window, true)
    and coalesce(height < lead(height, 1 ) over y_window, true)
    and coalesce(height < lag(height, 1) over x_window, true)
    and coalesce(height < lead(height, 1 ) over x_window, true)
    as is_min
  from
    map
  window
    y_window as (partition by y order by x),
    x_window as (partition by x order by y)
),
zone as (
  select
    x as orig_x, y as orig_y, x, y
  from
    low_points
  where is_min

  union

  select
    orig_x, orig_y, l.x, l.y
  from
    zone b
    cross join (values(-1, 0), (1, 0), (0, 1), (0, -1)) as delta(x, y)
    join low_points l on
      l.x = b.x + delta.x and l.y = b.y + delta.y
  where l.height < 9
),
area as (
  select
    count(*) as area
  from
    zone
  group by orig_x, orig_y
  order by area desc
  limit 3
),
biggest as (
  select row_number() over () as id, area from area
),
mult as (
  select 1 as pt, 1::bigint as acc
  union
  select pt+1 as pt, acc * area as acc
  from
    mult
    join biggest on pt=id
)
select
  mult.acc as "Answer!!"
from
  mult
order by pt desc
limit 1
;

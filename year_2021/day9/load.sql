
drop table if exists day9;

create table day9(line_number integer not null generated always as identity, line text);

\copy day9(line) from './input'

vacuum analyze day9;

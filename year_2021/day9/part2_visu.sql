-- to be executed with
-- psql -U postgres -d adventofcode --no-psqlrc -A -f <file>
with map as (
  select
    line_number as y,
    x,
    char::int as height
  from
    day9,
    regexp_split_to_table(line, '') with ordinality as _(char, x)
),
search as (
  select
    x,
    y,
    height,
    coalesce(height < lag(height, 1) over y_window, true)
    and coalesce(height < lead(height, 1 ) over y_window, true)
    and coalesce(height < lag(height, 1) over x_window, true)
    and coalesce(height < lead(height, 1 ) over x_window, true)
    as is_min
  from
    map
  window
    y_window as (partition by y order by x),
    x_window as (partition by x order by y)
)
select
string_agg(case
  when is_min then  chr(27)|| '[48;2;240;59;39m '  ||chr(27)||'[0m'
--'.'
  when height < 9 then chr(27)|| '[48;2;254;178;76m '  ||chr(27)||'[0m'
--'▪'
  else chr(27)|| '[48;2;255;237;160m '  ||chr(27)||'[0m'
end, '' order by x)
from
  search
  group by y
  order by y
;

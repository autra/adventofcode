with recursive
fold as (
  select
    row_number() over () as fold_id,
    m[1] as axis_name,
    m[2]::int as axis_value
  from
    day13,
    regexp_matches(line, 'fold along (\w)=(\d+)') m
  where
    line_number > (
      select
        line_number
      from
        day13
      where
        line = ''
    )
  order by
    line_number
), points_list as (
  select
    1 as fold_pt,
    split_part(line, ',', 1)::int as x,
    split_part(line, ',', 2)::int as y
  from
    day13
  where
    line_number < (
      select
        line_number
      from
        day13
      where
        line = ''
    )

  union all

  select
    distinct
    fold_pt + 1,
    case
      when f.axis_name = 'x' and x >= f.axis_value then
        2 * f.axis_value - x
      else
        x
    end as x,
    case
      when f.axis_name = 'y' and y >= f.axis_value then
        2 * f.axis_value - y
      else
        y
    end as y
  from
    points_list pts
    join fold f on fold_pt=fold_id
),
result as (
  select x,y from points_list where fold_pt=(select max(fold_pt) from points_list)
)
select
  -- replace . by ' ' and # by a utf-8 char for lisibility
  string_agg(case when pts.x is null then ' ' else '■' end, '' order by x.x) as answer
from
  generate_series(0, (select max(x) from result)) x(x)
  cross join generate_series(0, (select max(y) from result)) y(y)
  left join result pts on pts.x=x.x and pts.y=y.y
group by y.y
;


drop table if exists day13;

create table day13(line_number integer not null generated always as identity, line text);

\copy day13(line) from './input2'

vacuum analyze day13;

-- parsing...
with coords as (
  select
    split_part(line, ',', 1)::int as x,
    split_part(line, ',', 2)::int as y
  from
    day13
  where
    line_number < (
      select
        line_number
      from
        day13
      where
        line = ''
    )
),
fold as (
  select
    m[1] as axis_name,
    m[2]::int as axis_value
  from
    day13,
    regexp_matches(line, 'fold along (\w)=(\d+)') m
  where
    line_number > (
      select
        line_number
      from
        day13
      where
        line = ''
    )
  order by
    line_number
  limit 1
), new_points_list as (
  select
    distinct
    case
      when f.axis_name = 'x' and x > f.axis_value then
        2 * f.axis_value - x
      else
        x
    end as x,
    case
      when f.axis_name = 'y' and y > f.axis_value then
        2 * f.axis_value - y
      else
        y
    end as y
  from
    fold f
    cross join coords c1
)
select count(*) as "Answer!!" from new_points_list;

select
  count(*) as "Answer!!"
from
  day8,
  split_part(line, ' | ', 2) display,
  regexp_split_to_table(display, ' ') digit
where
  length(digit) = any(array[2, 3, 4, 7]);

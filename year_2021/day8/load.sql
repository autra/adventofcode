
drop table if exists day8;

create table day8(line_number integer not null generated always as identity, line text);

\copy day8(line) from './input'

vacuum analyze day8;

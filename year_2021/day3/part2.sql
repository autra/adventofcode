with recursive
o2_gen_rating(it, line) as (
  select 1 as it, line
  from day3

  union all

  (
    with current as (
      table o2_gen_rating
    )
    select it+1 as it, line
    from
      current,
      lateral (
        select
          ord,
          -- count number of 0 and 1 on each column
          count(nullif(bit, '0')) >= count(nullif(bit, '1')) as ok
        from
          current,
          unnest(regexp_split_to_array(line, '')) with ordinality as _(bit, ord)
        where
          ord=it
        group by ord
      ) counts
    where get_bit(line::bit(12), it - 1) = ok::int
    and it <= 12
  )
),
co2_scrubber_rating(it, line) as (
  select 1 as it, line
  from day3

  union all

  (
    with current as (
      table co2_scrubber_rating
    )
    select it+1 as it, line
    from
      current,
      lateral (
        select
          ord,
          -- count number of 0 and 1 on each column
          count(nullif(bit, '0')) >= count(nullif(bit, '1')) as ok
        from
          current,
          unnest(regexp_split_to_array(line, '')) with ordinality as _(bit, ord)
        where
          ord=it
        group by ord
      ) counts
    where get_bit(line::bit(12), it - 1) != ok::int
    and it <= 12

  )
)
select
  o.line::bit(12)::int * c.line::bit(12)::int as "Answer!!"
from
  co2_scrubber_rating c,
  o2_gen_rating o
order by c.it desc, o.it desc
limit 1;

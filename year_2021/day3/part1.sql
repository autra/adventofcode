with counts as (
  select
    ord,
    -- count number of 0 and 1 on each column
    count(nullif(bit, '0')) number_1,
    count(nullif(bit, '1')) number_0
  from
    day3,
    unnest(regexp_split_to_array(line, '')) with ordinality as _(bit, ord)
  group by ord
)
select
  string_agg((number_1 > number_0)::int::text, '' order by ord)::bit(12)::int
    * string_agg((number_0 > number_1)::int::text, '' order by ord)::bit(12)::int
    as "Answer!!"
from
  counts
;


drop table if exists day3;

create table day3(line_number integer not null generated always as identity, line text);

\copy day3(line) from './input2'

vacuum analyze day3;

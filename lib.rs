pub mod year_2022;
pub mod year_2023;
pub mod year_2024;

/// Generates
// BTreeMap::from([
///     (
///         1,
///         (
///             Box::new(year_2023::day1::part1 as fn(&[&str]) -> Box<dyn Display>),
///             Box::new(year_2023::day1::part2 as fn(&[&str]) -> Box<dyn Display>),
///         ),
///     ),
///     ...
/// ])
#[macro_export]
macro_rules! days{
    // () => {
    //     panic!("Day macro called without arg");

    // };
    ( $year: tt, ($($day: tt),* )) => {{
        paste! {
            BTreeMap::from([
                $((
                    $day,
                    (
                        Box::new([<year_$year>]::[<day$day>]::part1 as fn(&[&str]) -> Box<dyn Display>),
                        Box::new([<year_$year>]::[<day$day>]::part2 as fn(&[&str]) -> Box<dyn Display>),
                    )
                )),*
            ])
        }
    }};
}

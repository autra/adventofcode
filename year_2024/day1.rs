use std::collections::HashMap;
use std::fmt::Display;

pub fn parse_input(lines: &[&str]) -> (Vec<i32>, Vec<i32>) {
    let rows: Vec<Vec<&str>> = lines.iter().map(|s| s.split("   ").collect()).collect();
    // Extract columns and sort them independently
    let col1: Vec<_> = rows
        .iter()
        .map(|row| row[0].parse::<i32>().expect("Invalid number"))
        .collect();
    let col2: Vec<_> = rows
        .iter()
        .map(|row| row[1].parse::<i32>().expect("Invalid number"))
        .collect();
    (col1, col2)
}

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> usize {
    let (mut col1, mut col2) = parse_input(lines);

    col1.sort();
    col2.sort();

    // Calculate absolute differences between sorted columns
    let sum: i32 = col1
        .iter()
        .zip(col2.iter())
        .map(|(x, y)| (x - y).abs())
        .sum();
    sum as usize
}

fn _part2(lines: &[&str]) -> usize {
    let (col1, col2) = parse_input(lines);

    let counts = col2.iter().fold(HashMap::new(), |mut acc, &elem| {
        *acc.entry(elem).or_insert(0) += 1;
        acc
    });

    let sum: i32 = col1
        .iter()
        .map(|elem| elem * counts.get(&elem).unwrap_or(&0))
        .sum();
    sum as usize
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
3   4
4   3
2   5
1   3
3   9
3   3";

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 11);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part2(&input_vec), 31);
    }
}

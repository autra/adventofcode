use regex::Regex;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> usize {
    let re = Regex::new(r"mul\(([0-9]{1,3}),([0-9]{1,3})\)").unwrap();
    lines
        .iter()
        .map(|line| {
            let mut result = 0;
            for (_, [op1, op2]) in re.captures_iter(line).map(|c| c.extract()) {
                result += op1.parse::<usize>().unwrap() * op2.parse::<usize>().unwrap();
            }
            result
        })
        .sum()
}

fn is_activated(bounds: &[(usize, bool)], index: usize) -> bool {
    let mut is_activated = true;
    for (bound, activated) in bounds {
        if *bound <= index {
            is_activated = *activated;
        } else {
            return is_activated;
        }
    }
    is_activated
}

fn _part2(lines: &[&str]) -> usize {
    let full_line = lines.join("");
    let do_re = Regex::new(r"(do\(\)|don't\(\))").unwrap();
    let op_re = Regex::new(r"mul\(([0-9]{1,3}),([0-9]{1,3})\)").unwrap();
    // find do and don't
    let bounds: Vec<(usize, bool)> =
        do_re
            .find_iter(&full_line)
            .fold(vec![(0, true)], |mut acc, m| {
                let activated = acc.last().unwrap().1;
                if activated && m.as_str() == "don't()" {
                    acc.push((m.start(), false))
                } else if !activated && m.as_str() == "do()" {
                    acc.push((m.end(), true))
                }
                acc
            });
    op_re.captures_iter(&full_line).fold(0, |mut acc, capture| {
        if is_activated(&bounds, capture.get(0).unwrap().start()) {
            let op1 = capture.get(1).unwrap().as_str();
            let op2 = capture.get(2).unwrap().as_str();
            acc += op1.parse::<usize>().unwrap() * op2.parse::<usize>().unwrap();
        }
        acc
    })
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))";
    const INPUT2: &str = "\
xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))";

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 161);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT2.split('\n').collect();
        assert_eq!(_part2(&input_vec), 48);
    }
}

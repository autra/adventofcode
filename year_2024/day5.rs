use std::cmp::Ordering;
use std::fmt::Display;

fn cmp(rules: &[&(usize, usize)], a: &usize, b: &usize) -> Ordering {
    let rule: &(_, _) = rules
        .iter()
        .find(|r| r.0 == *a && r.1 == *b || r.0 == *b && r.1 == *a)
        .unwrap();
    if (&rule.0, &rule.1) == (a, b) {
        Ordering::Less
    } else {
        Ordering::Greater
    }
}

fn get_filtered_rules<'a>(
    update: &[usize],
    rules: &'a [(usize, usize)],
) -> Vec<&'a (usize, usize)> {
    rules
        .iter()
        .filter(|(left, right)| update.contains(left) && update.contains(right))
        .collect()
}

fn is_sorted(update: &[usize], rules: &[&(usize, usize)]) -> bool {
    update
        .iter()
        .is_sorted_by(|&a, &b| cmp(rules, a, b) == Ordering::Less)
}

fn parse(lines: &[&str]) -> (Vec<(usize, usize)>, Vec<Vec<usize>>) {
    let mut it = lines.iter();
    let mut rules: Vec<(usize, usize)> = vec![];
    while let Some(line) = it.next() {
        if *line == "" {
            break;
        }
        let mut insts = line.split('|');
        let first = insts.next().unwrap().parse::<usize>().unwrap();
        let last = insts.next().unwrap().parse::<usize>().unwrap();
        rules.push((first, last));
    }

    let mut updates: Vec<Vec<usize>> = vec![];
    while let Some(line) = it.next() {
        updates.push(
            line.split(',')
                .map(|s| s.parse::<usize>().unwrap())
                .collect(),
        );
    }
    (rules, updates)
}

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> usize {
    let (rules, updates) = parse(lines);
    updates
        .iter()
        .map(|u| {
            let filtered_rules: Vec<&(usize, usize)> = get_filtered_rules(u, &rules);
            if is_sorted(&u, &filtered_rules) {
                u[(u.len() - 1) / 2]
            } else {
                0
            }
        })
        .sum()
}

fn _part2(lines: &[&str]) -> usize {
    let (rules, mut updates) = parse(lines);
    updates
        .iter_mut()
        .map(|u| {
            let filtered_rules = get_filtered_rules(u, &rules);
            if is_sorted(&u, &filtered_rules) {
                0
            } else {
                u.sort_by(|a, b| cmp(&filtered_rules, a, b));
                u[(u.len() - 1) / 2]
            }
        })
        .sum()
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47";

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 143);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part2(&input_vec), 123);
    }
}

use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> usize {
    lines.iter().fold(0, |mut acc, line| {
        let numbers: Vec<i32> = line
            .split_whitespace()
            .filter_map(|s| s.parse().ok())
            .collect();
        let sign = numbers[0] - numbers[1];
        let differences: Vec<i32> = numbers.windows(2).map(|w| w[0] - w[1]).collect();
        if differences.iter().all(|d| d * sign > 0 && d.abs() < 4) {
            acc = acc + 1;
        }
        acc
    })
}

fn calculate_validity(report: &[i32]) -> bool {
    let sign = report[0] - report[1];
    let differences: Vec<i32> = report.windows(2).map(|w| w[0] - w[1]).collect();
    return differences.iter().all(|d| d * sign > 0 && d.abs() < 4);
}

fn part2_isvalid(report: Vec<i32>) -> bool {
    if calculate_validity(&report) {
        return true;
    }

    for i in 0..report.len() {
        let mut new_report = report.clone();
        new_report.remove(i);
        if calculate_validity(&new_report) {
            return true;
        }
    }
    return false;
}

fn _part2(lines: &[&str]) -> usize {
    lines.iter().fold(0, |mut acc, line| {
        let numbers: Vec<i32> = line
            .split_whitespace()
            .filter_map(|s| s.parse().ok())
            .collect();
        if part2_isvalid(numbers) {
            acc += 1
        }
        acc
    })
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9";

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 2);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part2(&input_vec), 4);
    }
}

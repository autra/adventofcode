use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

#[derive(Debug)]
struct Direction {
    x: isize,
    y: isize,
}

impl Direction {
    fn mul(&self, step: isize) -> Direction {
        Direction {
            x: self.x * step,
            y: self.y * step,
        }
    }
}

fn check_word_from_position_and_direction(
    letter_map: &Vec<Vec<char>>,
    word: &str,
    direction: &Direction,
    x: usize,
    y: usize,
) -> bool {
    let word_array: Vec<char> = word.chars().collect();
    for step in 0..4 {
        let offset = direction.mul(step as isize);
        let x_offset = x as isize + offset.x;
        if x_offset < 0 {
            return false;
        }
        let x_offset = x_offset as usize;
        let y_offset = y as isize + offset.y;
        if y_offset < 0 {
            return false;
        }
        let y_offset = y_offset as usize;
        if let Some(line) = letter_map.get(x_offset) {
            if let Some(letter) = line.get(y_offset) {
                if *letter == word_array[step] {
                    continue;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
    true
}

fn _part1(lines: &[&str]) -> usize {
    let letter_map: Vec<Vec<char>> = lines.iter().map(|line| line.chars().collect()).collect();
    // horizontal
    let mut count = 0;
    let directions = vec![
        Direction { x: 1, y: 0 },
        Direction { x: 0, y: 1 },
        Direction { x: 1, y: 1 },
        Direction { x: 1, y: -1 },
    ];
    for i in 0..letter_map.len() {
        for j in 0..letter_map[i].len() {
            for dir in &directions {
                for sign in [-1, 1] {
                    let dir = dir.mul(sign);
                    if check_word_from_position_and_direction(&letter_map, "XMAS", &dir, i, j) {
                        count += 1;
                    }
                }
            }
        }
    }
    count
}

fn _part2(lines: &[&str]) -> usize {
    let letter_map: Vec<Vec<char>> = lines.iter().map(|line| line.chars().collect()).collect();
    // horizontal
    let mut count = 0;
    for i in 1..(letter_map.len() - 1) {
        for j in 1..(letter_map[i].len() - 1) {
            if letter_map[i][j] != 'A' {
                continue;
            }
            if (letter_map[i - 1][j - 1] == 'M'
                && letter_map[i - 1][j + 1] == 'M'
                && letter_map[i + 1][j - 1] == 'S'
                && letter_map[i + 1][j + 1] == 'S')
                || (letter_map[i - 1][j - 1] == 'S'
                    && letter_map[i - 1][j + 1] == 'M'
                    && letter_map[i + 1][j - 1] == 'S'
                    && letter_map[i + 1][j + 1] == 'M')
                || (letter_map[i - 1][j - 1] == 'S'
                    && letter_map[i - 1][j + 1] == 'S'
                    && letter_map[i + 1][j - 1] == 'M'
                    && letter_map[i + 1][j + 1] == 'M')
                || (letter_map[i - 1][j - 1] == 'M'
                    && letter_map[i - 1][j + 1] == 'S'
                    && letter_map[i + 1][j - 1] == 'M'
                    && letter_map[i + 1][j + 1] == 'S')
            {
                count += 1;
            }
        }
    }
    count
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX";

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 18);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part2(&input_vec), 9);
    }
}

use std::{
    collections::HashMap,
    fmt::{Debug, Display},
};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct TryFromError;

#[derive(Debug, PartialEq, Eq, PartialOrd)]
enum HandType {
    HighCard,
    OnePair,
    TwoPair,
    ThreeOfAKind,
    FullHouse,
    FourOfAKind,
    FiveOfAKind,
}

#[derive(Debug, Hash, PartialEq, Eq, PartialOrd)]
enum Card {
    C2,
    C3,
    C4,
    C5,
    C6,
    C7,
    C8,
    C9,
    T,
    J,
    Q,
    K,
    A,
}

impl TryFrom<char> for Card {
    type Error = TryFromError;

    fn try_from(c: char) -> Result<Self, Self::Error> {
        match c {
            'J' => Ok(Card::J),
            '2' => Ok(Card::C2),
            '3' => Ok(Card::C3),
            '4' => Ok(Card::C4),
            '5' => Ok(Card::C5),
            '6' => Ok(Card::C6),
            '7' => Ok(Card::C7),
            '8' => Ok(Card::C8),
            '9' => Ok(Card::C9),
            'T' => Ok(Card::T),
            'Q' => Ok(Card::Q),
            'K' => Ok(Card::K),
            'A' => Ok(Card::A),
            _ => Err(TryFromError),
        }
    }
}

#[derive(Debug, Hash, PartialEq, Eq, PartialOrd)]
enum CardWithJoker {
    J,
    C2,
    C3,
    C4,
    C5,
    C6,
    C7,
    C8,
    C9,
    T,
    Q,
    K,
    A,
}

impl TryFrom<char> for CardWithJoker {
    type Error = TryFromError;

    fn try_from(c: char) -> Result<Self, Self::Error> {
        match c {
            'J' => Ok(CardWithJoker::J),
            '2' => Ok(CardWithJoker::C2),
            '3' => Ok(CardWithJoker::C3),
            '4' => Ok(CardWithJoker::C4),
            '5' => Ok(CardWithJoker::C5),
            '6' => Ok(CardWithJoker::C6),
            '7' => Ok(CardWithJoker::C7),
            '8' => Ok(CardWithJoker::C8),
            '9' => Ok(CardWithJoker::C9),
            'T' => Ok(CardWithJoker::T),
            'Q' => Ok(CardWithJoker::Q),
            'K' => Ok(CardWithJoker::K),
            'A' => Ok(CardWithJoker::A),
            _ => Err(TryFromError),
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd)]
struct Hand<C> {
    hand_type: HandType,
    cards: Box<[C; 5]>,
    bet: usize,
}

impl Hand<Card> {
    fn from(card_str: &str, bet: usize) -> Hand<Card> {
        let mut c = Vec::new();
        for card_char in card_str.chars() {
            c.push(Card::try_from(card_char).unwrap());
        }
        let cards: [Card; 5] = c.try_into().unwrap();

        Hand {
            hand_type: Hand::<Card>::detect_hand_type(&cards),
            cards: Box::new(cards),
            bet,
        }
    }

    fn detect_hand_type(cards: &[Card; 5]) -> HandType {
        let mut map = HashMap::new();
        for card in cards {
            if !map.contains_key(card) {
                map.insert(card, 1);
            } else {
                map.insert(card, map.get(card).unwrap() + 1);
            }
        }

        match map.len() {
            1 => HandType::FiveOfAKind,
            2 => {
                // could be four of a kind or full house
                if let Some(_) = map.values().find(|v| **v == 4) {
                    HandType::FourOfAKind
                } else {
                    HandType::FullHouse
                }
            }
            3 => {
                if let Some(_) = map.values().find(|v| **v == 3) {
                    HandType::ThreeOfAKind
                } else {
                    HandType::TwoPair
                }
            }
            4 => HandType::OnePair,
            5 => HandType::HighCard,
            _ => panic!("This is really odd.. Was there more than five cards in {cards:?}?"),
        }
    }
}

impl Hand<CardWithJoker> {
    fn from(card_str: &str, bet: usize) -> Hand<CardWithJoker> {
        let mut c = Vec::new();
        for card_char in card_str.chars() {
            c.push(CardWithJoker::try_from(card_char).unwrap());
        }
        let cards: [CardWithJoker; 5] = c.try_into().unwrap();

        Hand {
            hand_type: Hand::<CardWithJoker>::detect_hand_type(&cards),
            cards: Box::new(cards),
            bet,
        }
    }

    fn detect_hand_type(cards: &[CardWithJoker; 5]) -> HandType {
        let mut map: HashMap<&CardWithJoker, usize> = HashMap::new();
        for card in cards {
            if !map.contains_key(card) {
                map.insert(card, 1);
            } else {
                map.insert(card, map.get(card).unwrap() + 1);
            }
        }
        let mut freq: Vec<(&&CardWithJoker, &usize)> = map.iter().collect();
        freq.sort_by(|(_, va), (_, vb)| va.partial_cmp(vb).unwrap());
        let max_not_jocker = freq
            .iter()
            .rev()
            .find(|(k, _)| k != &&&CardWithJoker::J)
            .map(|v| v.1)
            .unwrap_or(&0);
        let joker_count: usize = *map.get(&CardWithJoker::J).unwrap_or(&0) as usize;
        match max_not_jocker + joker_count {
            1 => HandType::HighCard,
            2 => {
                if map.len() >= 4 {
                    HandType::OnePair
                } else {
                    // map.len cannot be 2 (otherwise count is min 3)
                    // so this is the case when map.len == 3
                    HandType::TwoPair
                }
            }
            3 => {
                if joker_count == 2 {
                    HandType::ThreeOfAKind
                } else if joker_count == 1 {
                    if map.len() == 4 {
                        // 2 of one card + J + 2 other different card
                        HandType::ThreeOfAKind
                    } else {
                        // map.len() == 3, cannot be 2
                        // 2 X, 1J, 2Y : Full
                        HandType::FullHouse
                    }
                } else {
                    // joker_count = 0
                    if map.len() == 2 {
                        HandType::FullHouse
                    } else {
                        HandType::ThreeOfAKind
                    }
                }
            }
            4 => HandType::FourOfAKind,
            5 => HandType::FiveOfAKind,
            _ => panic!("Too many values"),
        }
    }
}

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> usize {
    // parse cards
    let mut hands = Vec::new();
    for line in lines {
        let mut s = line.split(" ");
        hands.push(Hand::<Card>::from(
            s.next().unwrap(),
            s.next().unwrap().parse().unwrap(),
        ));
    }
    hands.sort_by(|a, b| a.partial_cmp(b).unwrap());
    let mut acc = 0;
    for (i, hand) in hands.iter().enumerate() {
        acc += (i + 1) * hand.bet;
    }
    acc
}

fn _part2(lines: &[&str]) -> usize {
    // parse cards
    let mut hands = Vec::new();
    for line in lines {
        let mut s = line.split(" ");
        hands.push(Hand::<CardWithJoker>::from(
            s.next().unwrap(),
            s.next().unwrap().parse().unwrap(),
        ));
    }
    hands.sort_by(|a, b| a.partial_cmp(b).unwrap());
    let mut acc = 0;
    for (i, hand) in hands.iter().enumerate() {
        acc += (i + 1) * hand.bet;
    }
    acc
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483";

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 6440);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part2(&input_vec), 5905);
    }

    #[test]
    fn test_detect_hand_type_with_joker() {
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::C3,
                CardWithJoker::C2,
                CardWithJoker::T,
                CardWithJoker::C3,
                CardWithJoker::K,
            ]),
            HandType::OnePair
        );

        // 5 different types
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::C3,
                CardWithJoker::C2,
                CardWithJoker::T,
                CardWithJoker::C4,
                CardWithJoker::K,
            ]),
            HandType::HighCard
        );
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::C3,
                CardWithJoker::C2,
                CardWithJoker::T,
                CardWithJoker::C4,
                CardWithJoker::J,
            ]),
            HandType::OnePair
        );

        // 4 different types
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::C3,
                CardWithJoker::C2,
                CardWithJoker::T,
                CardWithJoker::C3,
                CardWithJoker::K,
            ]),
            HandType::OnePair
        );
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::C3,
                CardWithJoker::C2,
                CardWithJoker::T,
                CardWithJoker::C3,
                CardWithJoker::J,
            ]),
            HandType::ThreeOfAKind
        );
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::J,
                CardWithJoker::C2,
                CardWithJoker::T,
                CardWithJoker::J,
                CardWithJoker::K,
            ]),
            HandType::ThreeOfAKind
        );

        // 3 different types
        // 3 - 1 - 1
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::K,
                CardWithJoker::C2,
                CardWithJoker::T,
                CardWithJoker::T,
                CardWithJoker::T,
            ]),
            HandType::ThreeOfAKind
        );
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::J,
                CardWithJoker::C2,
                CardWithJoker::T,
                CardWithJoker::T,
                CardWithJoker::T,
            ]),
            HandType::FourOfAKind
        );
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::K,
                CardWithJoker::C2,
                CardWithJoker::J,
                CardWithJoker::J,
                CardWithJoker::J,
            ]),
            HandType::FourOfAKind
        );
        // 2 - 2 - 1
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::K,
                CardWithJoker::K,
                CardWithJoker::C2,
                CardWithJoker::Q,
                CardWithJoker::Q,
            ]),
            HandType::TwoPair
        );
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::K,
                CardWithJoker::K,
                CardWithJoker::C2,
                CardWithJoker::J,
                CardWithJoker::J,
            ]),
            HandType::FourOfAKind
        );
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::K,
                CardWithJoker::K,
                CardWithJoker::J,
                CardWithJoker::Q,
                CardWithJoker::Q,
            ]),
            HandType::FullHouse
        );

        // 2 different types
        // 4 - 1
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::K,
                CardWithJoker::K,
                CardWithJoker::K,
                CardWithJoker::K,
                CardWithJoker::Q,
            ]),
            HandType::FourOfAKind
        );
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::K,
                CardWithJoker::K,
                CardWithJoker::K,
                CardWithJoker::K,
                CardWithJoker::J,
            ]),
            HandType::FiveOfAKind
        );
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::J,
                CardWithJoker::J,
                CardWithJoker::J,
                CardWithJoker::J,
                CardWithJoker::K,
            ]),
            HandType::FiveOfAKind
        );
        // 3 - 2
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::J,
                CardWithJoker::J,
                CardWithJoker::J,
                CardWithJoker::K,
                CardWithJoker::K,
            ]),
            HandType::FiveOfAKind
        );
        assert_eq!(
            Hand::<CardWithJoker>::detect_hand_type(&[
                CardWithJoker::Q,
                CardWithJoker::Q,
                CardWithJoker::Q,
                CardWithJoker::K,
                CardWithJoker::K,
            ]),
            HandType::FullHouse
        );
    }
}

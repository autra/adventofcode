use std::fmt::Display;

static NUM_WORDS: &[&str] = &[
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> usize {
    let mut acc = 0;
    for line in lines {
        let mut number_str = String::new();
        for char in line.chars() {
            if char.is_ascii_digit() {
                number_str.push(char);
                break;
            }
        }
        for char in line.chars().rev() {
            if char.is_ascii_digit() {
                number_str.push(char);
                break;
            }
        }
        if let Ok(number) = number_str.parse::<usize>() {
            acc += number;
        }
    }
    return acc;
}

fn parse_number_at_beginning(input: &str) -> Option<usize> {
    if input.chars().nth(0).unwrap().is_ascii_digit() {
        // check if current char is a number
        input
            .chars()
            .nth(0)
            .map(|o| o.to_digit(10).unwrap() as usize)
    } else {
        // get the position of the number that may start the &str
        // if not returns None
        NUM_WORDS
            .iter()
            .position(|num_word| input.starts_with(num_word))
            .map(|v| v + 1)
    }
}

fn _part2(lines: &[&str]) -> usize {
    let mut acc: usize = 0;
    for line in lines {
        for i in 0..line.len() {
            if let Some(num) = parse_number_at_beginning(&line[i..]) {
                acc += 10 * num;
                break;
            }
        }
        for i in (0..line.len()).rev() {
            if let Some(num) = parse_number_at_beginning(&line[i..]) {
                acc += num;
                break;
            }
        }
    }
    return acc;
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part1(&input_vec), 142);
    }
    #[test]
    fn test_part2() {
        let input = "\
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part2(&input_vec), 281);
    }
}

use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn derive_vals(values: &[isize]) -> Vec<isize> {
    values
        .windows(2)
        .map(|arr| (arr[1] as isize - arr[0] as isize))
        .collect()
}

fn extrapolate(values: &[isize]) -> (isize, isize) {
    if values.iter().all(|v| *v == 0) {
        (0, 0)
    } else {
        let extrapolated = extrapolate(&derive_vals(values));
        (
            values.first().unwrap() - extrapolated.0,
            extrapolated.1 + values.last().unwrap(),
        )
    }
}

fn _part1(lines: &[&str]) -> isize {
    let mut acc = 0;
    for line in lines {
        let values: Vec<isize> = line.split(' ').map(|s| s.parse().unwrap()).collect();
        acc += extrapolate(&values).1;
    }
    acc
}

fn _part2(lines: &[&str]) -> isize {
    let mut acc = 0;
    for line in lines {
        let values: Vec<isize> = line.split(' ').map(|s| s.parse().unwrap()).collect();
        acc += extrapolate(&values).0;
    }
    acc
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45";

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 114);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part2(&input_vec), 2);
    }
}

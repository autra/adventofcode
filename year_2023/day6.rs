use std::fmt::Display;
use std::iter::zip;

use regex::Regex;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

/// distance = speed * time
/// if we take into account the wait time we have:
/// - time = T (total time) - x (waiting time)
/// - and speed = x (waiting time)
///
/// So:
/// D = x * (T - x) and that should be greater than the record R
///
/// after development:
///
/// -x^2 + T*x - R > 0
///
/// this is a 2nd degree polynom, so it has the sign of the x² factor outside of roots. So it's
/// positive *between* the 2 solutions.
///
/// So we just have to calculate the 2 solutions and diff them (with a *little* step about rounding
/// correctly, and avoid the precise solution, because we would then just reproduce the record
/// without beating it)
///
fn calculate_solution(time: usize, record: usize) -> usize {
    let t = time as f64;
    let r = record as f64;
    let x1 = (t + (t * t - 4.0 * r).sqrt()) / 2.0;
    let x2 = (t - (t * t - 4.0 * r).sqrt()) / 2.0;
    let t2 = (x2 + 1.0).floor() as isize;
    let t1 = (x1 - 1.0).ceil() as isize;
    (t1 - t2 + 1) as usize
}

fn _part1(lines: &[&str]) -> usize {
    let line_re = Regex::new(r"(\d+)").unwrap();

    let mut times: Vec<usize> = Vec::new();
    for (_, [time]) in line_re.captures_iter(lines[0]).map(|c| c.extract()) {
        times.push(time.parse().unwrap());
    }
    let mut records: Vec<usize> = Vec::new();
    for (_, [record]) in line_re.captures_iter(lines[1]).map(|c| c.extract()) {
        records.push(record.parse().unwrap());
    }

    let mut acc = 1;
    for (time, record) in zip(times, records) {
        let sol = calculate_solution(time, record);
        acc = sol * acc;
    }
    acc
}

fn _part2(lines: &[&str]) -> usize {
    let line_re = Regex::new(r"(\d+)").unwrap();

    let mut time = String::new();
    for (_, [time_part]) in line_re.captures_iter(lines[0]).map(|c| c.extract()) {
        time.push_str(time_part);
    }
    let mut record = String::new();
    for (_, [record_part]) in line_re.captures_iter(lines[1]).map(|c| c.extract()) {
        record.push_str(record_part);
    }
    let time: usize = time.parse().unwrap();
    let record: usize = record.parse().unwrap();

    calculate_solution(time, record)
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
Time:      7  15   30
Distance:  9  40  200";

    #[test]
    fn test_part1_1() {
        const INPUT: &str = "\
Time:      7
Distance:  9";
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 4);
    }

    #[test]
    fn test_part1_2() {
        const INPUT: &str = "\
Time:      15
Distance:  40";
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 8);
    }

    #[test]
    fn test_part1_3() {
        const INPUT: &str = "\
Time:      30
Distance:  200";
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 9);
    }

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 288);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part2(&input_vec), 71503);
    }
}

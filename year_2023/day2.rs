use regex::Regex;
use std::cmp::max;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> usize {
    let id_game_re = Regex::new(r"Game (\d+): (.+)").unwrap();
    let num_color_re = Regex::new(r"(\d+) (\w+)").unwrap();
    let mut sum_possible = 0;
    for line in lines {
        let mut possible = true;
        let caps = id_game_re.captures(line).unwrap();
        let id = &caps[1].parse::<usize>().unwrap();
        let game = &caps[2];
        let sets = game.split(";");
        for set in sets {
            for (_, [number, color]) in num_color_re.captures_iter(set.trim()).map(|c| c.extract())
            {
                let number = number.parse::<usize>().unwrap();
                possible &= match color {
                    "red" => number <= 12,
                    "green" => number <= 13,
                    "blue" => number <= 14,
                    _ => panic!("Unknown color {color}"),
                };
            }
        }
        if possible {
            sum_possible += id;
        }
    }

    sum_possible
}

fn _part2(lines: &[&str]) -> usize {
    let id_game_re = Regex::new(r"Game \d+: (.+)").unwrap();
    let num_color_re = Regex::new(r"(\d+) (\w+)").unwrap();
    let mut sum_possible = 0;
    for line in lines {
        let mut min_green = 0;
        let mut min_blue = 0;
        let mut min_red = 0;
        let caps = id_game_re.captures(line).unwrap();
        let game = &caps[1];
        let sets = game.split(";");
        for set in sets {
            for (_, [number, color]) in num_color_re.captures_iter(set.trim()).map(|c| c.extract())
            {
                let number = number.parse::<usize>().unwrap();
                match color {
                    "red" => min_red = max(min_red, number),
                    "green" => min_green = max(min_green, number),
                    "blue" => min_blue = max(min_blue, number),
                    _ => panic!("Unknown color {color}"),
                };
            }
        }
        sum_possible += min_green * min_blue * min_red;
    }

    sum_possible
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part1(&input_vec), 8);
    }

    #[test]
    fn test_part2() {
        let input = "\
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part2(&input_vec), 2286);
    }
}

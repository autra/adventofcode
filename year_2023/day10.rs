use std::fmt::Display;

// NOTE the coordinate system is x to the right, y from top to bottom
pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

#[derive(Debug)]
enum Cardinal {
    North,
    South,
    East,
    West,
}

#[derive(Debug)]
struct CurrentPosition {
    provenance: Cardinal,
    x: usize,
    y: usize,
}

#[derive(Debug, PartialEq)]
struct Point {
    x: usize,
    y: usize,
}

fn get_next_points(map_yx: &Vec<Vec<char>>, current_point: CurrentPosition) -> CurrentPosition {
    let current_char = map_yx[current_point.y][current_point.x];
    let current_provenance = current_point.provenance;

    // for this char, get what it connects
    // so get the Cardinal destination
    match current_provenance {
        Cardinal::East => match current_char {
            '-' => CurrentPosition {
                provenance: Cardinal::East,
                x: current_point.x - 1,
                y: current_point.y,
            },
            'L' => CurrentPosition {
                provenance: Cardinal::South,
                x: current_point.x,
                y: current_point.y - 1,
            },
            'F' => CurrentPosition {
                provenance: Cardinal::North,
                x: current_point.x,
                y: current_point.y + 1,
            },
            _ => panic!("Impossible situation {current_provenance:?} - {current_char:?}"),
        },
        Cardinal::West => match current_char {
            '-' => CurrentPosition {
                provenance: Cardinal::West,
                x: current_point.x + 1,
                y: current_point.y,
            },
            '7' => CurrentPosition {
                provenance: Cardinal::North,
                x: current_point.x,
                y: current_point.y + 1,
            },
            'J' => CurrentPosition {
                provenance: Cardinal::South,
                x: current_point.x,
                y: current_point.y - 1,
            },
            _ => panic!("Impossible situation {current_provenance:?} - {current_char:?}"),
        },
        Cardinal::North => match current_char {
            '|' => CurrentPosition {
                provenance: Cardinal::North,
                x: current_point.x,
                y: current_point.y + 1,
            },
            'J' => CurrentPosition {
                provenance: Cardinal::East,
                x: current_point.x - 1,
                y: current_point.y,
            },
            'L' => CurrentPosition {
                provenance: Cardinal::West,
                x: current_point.x + 1,
                y: current_point.y,
            },
            _ => panic!("Impossible situation {current_provenance:?} - {current_char:?}"),
        },
        Cardinal::South => match current_char {
            '|' => CurrentPosition {
                provenance: Cardinal::South,
                x: current_point.x,
                y: current_point.y - 1,
            },
            '7' => CurrentPosition {
                provenance: Cardinal::East,
                x: current_point.x - 1,
                y: current_point.y,
            },
            'F' => CurrentPosition {
                provenance: Cardinal::West,
                x: current_point.x + 1,
                y: current_point.y,
            },
            _ => panic!("Impossible situation {current_provenance:?} - {current_char:?}"),
        },
    }
}

fn parse_map(lines: &[&str]) -> Vec<Vec<char>> {
    let mut map_yx = Vec::new();
    for line in lines {
        let chars: Vec<char> = line.chars().collect();
        map_yx.push(chars);
    }
    map_yx
}

fn find_s(map_yx: &Vec<Vec<char>>) -> Point {
    let mut x = None;
    let mut y = None;
    for (yy, map_x) in map_yx.iter().enumerate() {
        for (xx, val) in map_x.iter().enumerate() {
            if val == &'S' {
                y = Some(yy);
                x = Some(xx);
                break;
            }
        }
    }

    let x = x.expect("Couldn't find S");
    let y = y.expect("Couldn't find S");
    Point { x, y }
}

fn find_path(map_yx: &Vec<Vec<char>>, s_pt: Point) -> Vec<Point> {
    let mut current_points = Vec::new();
    let mut path = Vec::new();
    let x = s_pt.x;
    let y = s_pt.y;
    path.push(s_pt);
    // find the first next tile
    for (xoffset, yoffset, provenance, possible_vals) in [
        (-1 as isize, 0, Cardinal::East, ['-', 'L', 'F']),
        (1, 0, Cardinal::West, ['-', '7', 'J']),
        (0, 1, Cardinal::North, ['|', 'L', 'J']),
        (0, -1 as isize, Cardinal::South, ['|', '7', 'F']),
    ] {
        let (nexty, nextx) = (
            (y as isize + yoffset).try_into().ok(),
            (x as isize + xoffset).try_into().ok(),
        );

        if let (Some(nexty), Some(nextx)) = (nexty, nextx) {
            let nextc: char = (&map_yx[nexty] as &Vec<char>)[nextx];
            if possible_vals.contains(&nextc) {
                path.push(Point { x: nextx, y: nexty });
                current_points.push(CurrentPosition {
                    provenance,
                    y: nexty,
                    x: nextx,
                });
            }
        }
    }
    assert_eq!(current_points.len(), 2);
    let mut current_points: (CurrentPosition, CurrentPosition) =
        (current_points.remove(0), current_points.remove(0));
    // note: it's impossible to finish at step 1.
    loop {
        let next_points_0 = get_next_points(&map_yx, current_points.0);
        let next_points_1 = get_next_points(&map_yx, current_points.1);
        current_points = (next_points_0, next_points_1);
        if current_points.0.x == current_points.1.x && current_points.0.y == current_points.1.y {
            path.push(Point {
                x: current_points.0.x,
                y: current_points.0.y,
            });
            // we've reached the same point
            break;
        } else {
            path.push(Point {
                x: current_points.0.x,
                y: current_points.0.y,
            });
            path.push(Point {
                x: current_points.1.x,
                y: current_points.1.y,
            });
        }
    }
    path
}

fn _part1(lines: &[&str]) -> usize {
    let map_yx: Vec<Vec<char>> = parse_map(lines);
    let s_pt = find_s(&map_yx);

    let path = find_path(&map_yx, s_pt);
    path.len() / 2
}

fn is_crossing(last_crossed_with: Option<char>, c: char) -> bool {
    if let Some(lcw) = last_crossed_with {
        if c == '-' {
            false
        } else {
            !(lcw == 'L' && c == '7' || lcw == 'F' && c == 'J')
        }
    } else {
        true
    }
}

fn _part2(lines: &[&str]) -> usize {
    let mut map_yx: Vec<Vec<char>> = parse_map(lines);
    let s_pt = find_s(&map_yx);

    // what is really s?
    let mut real_s = 'S';
    if s_pt.x > 0 && ['-', 'L', '7'].contains(&map_yx[s_pt.y][s_pt.x - 1]) {
        // from west
        if ['-', '7', 'J'].contains(&map_yx[s_pt.y][s_pt.x + 1]) {
            // going east
            real_s = '-';
        } else if s_pt.y > 0 && ['|', 'F', '7'].contains(&map_yx[s_pt.y - 1][s_pt.x]) {
            // going north
            real_s = 'J';
        } else if ['|', 'L', 'J'].contains(&map_yx[s_pt.y + 1][s_pt.x]) {
            // going south
            real_s = '7';
        }
    } else if s_pt.y > 0 && ['|', '7', 'F'].contains(&map_yx[s_pt.y - 1][s_pt.x]) {
        // from north
        if ['|', 'L', 'J'].contains(&map_yx[s_pt.y + 1][s_pt.x]) {
            // going south
            real_s = '|';
        } else if ['-', '7', 'J'].contains(&map_yx[s_pt.y][s_pt.x + 1]) {
            // going east
            real_s = 'L';
        }
        // going west already handled
    } else if ['-', 'J', '7'].contains(&map_yx[s_pt.y][s_pt.x + 1]) {
        // from east
        if ['|', 'L', 'J'].contains(&map_yx[s_pt.y + 1][s_pt.x]) {
            //  going south
            real_s = 'F';
        }
    }
    map_yx[s_pt.y][s_pt.x] = real_s;
    let path = find_path(&map_yx, s_pt);

    let mut count = 0;
    for (y, line) in map_yx.iter().enumerate() {
        let mut inside = false;
        let mut last_crossed_with = None;
        for (x, c) in line.iter().enumerate() {
            if path.contains(&Point { x, y }) {
                // we flip only if we are not currently walking an edge
                if is_crossing(last_crossed_with, *c) {
                    inside = !inside;
                    last_crossed_with = Some(*c);
                }
                continue;
            }
            if inside {
                count = count + 1;
            }
        }
    }

    // find a point on the outside that is *not* inside the path
    count
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
.....
.S-7.
.|.|.
.L-J.
.....";

    const INPUT2: &str = "\
..F7.
.FJ|.
SJ.L7
|F--J
LJ...";

    const INPUT3: &str = "\
7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ";

    const INPUT4: &str = "\
...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........";

    const INPUT5: &str = "\
.F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...";

    const INPUT6: &str = "\
FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L";

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 4);
        let input_vec: Vec<&str> = INPUT2.split('\n').collect();
        assert_eq!(_part1(&input_vec), 8);
        let input_vec: Vec<&str> = INPUT3.split('\n').collect();
        assert_eq!(_part1(&input_vec), 8);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part2(&input_vec), 1);
        let input_vec: Vec<&str> = INPUT2.split('\n').collect();
        assert_eq!(_part2(&input_vec), 1);
        let input_vec: Vec<&str> = INPUT3.split('\n').collect();
        assert_eq!(_part2(&input_vec), 1);
        let input_vec: Vec<&str> = INPUT4.split('\n').collect();
        assert_eq!(_part2(&input_vec), 4);
        let input_vec: Vec<&str> = INPUT5.split('\n').collect();
        assert_eq!(_part2(&input_vec), 8);
        let input_vec: Vec<&str> = INPUT6.split('\n').collect();
        assert_eq!(_part2(&input_vec), 10);
    }
}

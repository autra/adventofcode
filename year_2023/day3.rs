use regex::{Match, Regex};
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> usize {
    let number_re = Regex::new(r"(\d+)").unwrap();
    let something_re = Regex::new(r"[^\d.]").unwrap();
    let mut something: Vec<Vec<usize>> = Vec::new();
    // store all the ranges for this line
    // find all the positions of "something"
    for l in lines.iter() {
        something.push(something_re.find_iter(l).map(|m| m.start()).collect());
    }
    let mut acc = 0;
    for (i, l) in lines.iter().enumerate() {
        for m in number_re.find_iter(l) {
            let mut touches = false;
            let i = i as isize;
            for j in i - 1..i + 2 {
                if j < 0 || j >= lines.len() as isize {
                    continue;
                }
                let j = j as usize;
                touches |= something.get(j).unwrap().iter().any(|x| {
                    let x = *x as isize;
                    x >= (m.start() as isize - 1) && x <= m.end() as isize
                });
            }
            if touches {
                acc += m.as_str().parse::<usize>().unwrap();
            }
        }
    }
    return acc;
}

fn _part2(lines: &[&str]) -> usize {
    let mut acc = 0;
    let number_re = Regex::new(r"(\d+)").unwrap();
    let something_re = Regex::new(r"[^\d.]").unwrap();

    let mut numbers: Vec<Vec<Match>> = Vec::new();
    for l in lines.iter() {
        numbers.push(number_re.find_iter(l).collect());
    }
    // iterate over again to find parts
    for (i, l) in lines.iter().enumerate() {
        for m in something_re.find_iter(l) {
            let mut touching_numbers: Vec<&str> = Vec::new();
            let i = i as isize;
            let position = m.start();
            for j in i - 1..i + 2 {
                if j < 0 || j >= lines.len() as isize {
                    continue;
                }
                let j = j as usize;
                for num in numbers
                    .get(j)
                    .unwrap()
                    .iter()
                    .filter(|m| {
                        let position = position as isize;
                        position >= (m.start() as isize - 1) && position <= m.end() as isize
                    })
                    .map(|m| m.as_str())
                {
                    touching_numbers.push(num);
                }
            }
            if touching_numbers.len() == 2 {
                // multiply
                acc += touching_numbers[0].parse::<usize>().unwrap()
                    * touching_numbers[1].parse::<usize>().unwrap();
            }
        }
    }
    return acc;
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part1(&input_vec), 4361);
    }

    #[test]
    fn test_part2() {
        let input = "\
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part2(&input_vec), 467835);
    }
}

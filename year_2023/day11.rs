use std::cmp::{max, min};
use std::fmt::Display;

#[derive(Debug)]
struct Point {
    x: usize,
    y: usize,
}

impl Point {
    fn distance(
        &self,
        other: &Point,
        empty_x: &[usize],
        empty_y: &[usize],
        expansion_factor: usize,
    ) -> usize {
        // distance without expansion
        let distance =
            (self.x as isize - other.x as isize).abs() + (self.y as isize - other.y as isize).abs();
        let empty_lines_between = empty_y
            .iter()
            .filter(|e| *e > min(&self.y, &other.y) && *e < max(&self.y, &other.y))
            .map(|e| *e)
            .collect::<Vec<usize>>();
        let empty_columns_between = empty_x
            .iter()
            .filter(|e| *e > min(&self.x, &other.x) && **e < max(self.x, other.x))
            .map(|e| *e)
            .collect::<Vec<usize>>();
        let real_d = distance as usize
            + expansion_factor * (empty_lines_between.len() + empty_columns_between.len());
        real_d
    }
}

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn expand(map_yx: &Vec<Vec<char>>) -> (Vec<usize>, Vec<usize>) {
    // expand y axis
    let mut empty_y = Vec::new();
    let mut empty_x = Vec::new();
    for (i, line) in map_yx.iter().enumerate() {
        if !line.contains(&'#') {
            empty_y.push(i)
        }
    }
    // expand on x axis
    for i in 0..map_yx[0].len() {
        let mut is_empty = true;
        for j in 0..map_yx.len() {
            is_empty &= map_yx[j][i] == '.';
        }
        if is_empty {
            empty_x.push(i);
        }
    }
    (empty_x, empty_y)
}

fn soluce(lines: &[&str], expansion_factor: usize) -> usize {
    let map_yx: Vec<Vec<char>> = lines.iter().map(|l| l.chars().collect()).collect();
    let (empty_x, empty_y) = expand(&map_yx);
    let mut galaxies = Vec::new();
    for (y, line) in map_yx.into_iter().enumerate() {
        for (x, c) in line.iter().enumerate() {
            if c == &'#' {
                galaxies.push(Point { x, y });
            }
        }
    }
    let mut acc = 0;
    for i in 0..galaxies.len() {
        for j in (i + 1)..galaxies.len() {
            acc += galaxies[i].distance(&galaxies[j], &empty_x, &empty_y, expansion_factor);
        }
    }
    acc
}

fn _part1(lines: &[&str]) -> usize {
    soluce(lines, 1)
}

fn _part2(lines: &[&str]) -> usize {
    let map_yx: Vec<Vec<char>> = lines.iter().map(|l| l.chars().collect()).collect();
    let (empty_x, empty_y) = expand(&map_yx);
    let mut galaxies = Vec::new();
    for (y, line) in map_yx.into_iter().enumerate() {
        for (x, c) in line.iter().enumerate() {
            if c == &'#' {
                galaxies.push(Point { x, y });
            }
        }
    }
    let mut acc = 0;
    for i in 0..galaxies.len() {
        for j in (i + 1)..galaxies.len() {
            acc += galaxies[i].distance(&galaxies[j], &empty_x, &empty_y, 1000000 - 1);
        }
    }
    acc
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....";

    const INPUT_SIMPLE: &str = "#.#";

    #[test]
    fn test_expand() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        let map_yx: Vec<Vec<char>> = input_vec.iter().map(|l| l.chars().collect()).collect();
        let (empty_x, empty_y) = expand(&map_yx);
        assert_eq!(empty_y, vec![3, 7]);
        assert_eq!(empty_x, vec![2, 5, 8]);
    }

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 374);
    }

    #[test]
    fn test_soluce_simple() {
        let input_vec: Vec<&str> = INPUT_SIMPLE.split('\n').collect();
        assert_eq!(soluce(&input_vec, 1), 3);
    }
    #[test]
    fn test_soluce() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(soluce(&input_vec, 1), 374);
        assert_eq!(soluce(&input_vec, 9), 1030);
        assert_eq!(soluce(&input_vec, 99), 8410);
    }
}

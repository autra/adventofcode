use std::{collections::HashMap, fmt::Display};

use regex::Regex;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

#[derive(Debug)]
enum Command {
    L,
    R,
}

impl TryFrom<char> for Command {
    type Error = String;
    fn try_from(c: char) -> Result<Self, <Self as TryFrom<char>>::Error> {
        match c {
            'L' => Ok(Command::L),
            'R' => Ok(Command::R),
            _ => Err(format!("The char {c} is not a command")),
        }
    }
}

#[derive(Debug)]
struct Paths<'a> {
    left: &'a str,
    right: &'a str,
}

impl<'a> Paths<'a> {
    fn get_next(&self, command: &Command) -> &str {
        match command {
            Command::L => &self.left,
            Command::R => &self.right,
        }
    }
}

fn _part1(lines: &[&str]) -> usize {
    let commands: Vec<Command> = lines[0].chars().map(|c| c.try_into().unwrap()).collect();
    let path_re = Regex::new(r"^([[:upper:]]+) = \(([[:upper:]]+), ([[:upper:]]+)\)$").unwrap();

    let mut current_position = "AAA";
    let mut c_it = commands.iter().cycle();

    let mut path_map = HashMap::new();
    for line in lines.iter().skip(2) {
        let (_, [orig, left, right]) = path_re.captures(line).unwrap().extract();
        path_map.insert(orig, Paths { left, right });
    }
    let mut count = 0;
    while current_position != "ZZZ" {
        let command = c_it.next().unwrap();
        current_position = path_map.get(current_position).unwrap().get_next(command);
        count += 1;
    }
    count
}

fn _part2(lines: &[&str]) -> usize {
    let commands: Vec<Command> = lines[0].chars().map(|c| c.try_into().unwrap()).collect();
    let path_re =
        Regex::new(r"^([[:upper:]\d]+) = \(([[:upper:]\d]+), ([[:upper:]\d]+)\)$").unwrap();

    let mut c_it = commands.iter().cycle();

    let mut path_map = HashMap::new();
    for line in lines.iter().skip(2) {
        let (_, [orig, left, right]) = path_re.captures(line).unwrap().extract();
        path_map.insert(orig, Paths { left, right });
    }
    let current_positions: Vec<&str> = path_map
        .keys()
        .filter(|k| k.ends_with('A'))
        .map(|v| *v)
        .collect();
    let mut z_count: Vec<usize> = Vec::new();
    for starting_position in current_positions {
        let mut current_position = starting_position;
        let mut count = 0;
        while !current_position.ends_with('Z') {
            let command = c_it.next().unwrap();
            current_position = path_map.get(current_position).unwrap().get_next(command);
            count += 1;
        }
        z_count.push(count);
    }
    z_count
        .iter()
        .fold(1, |acc, elem| num::integer::lcm(acc, *elem))
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
RL

AAA = (BBB, CCC)
BBB = (DDD, EEE)
CCC = (ZZZ, GGG)
DDD = (DDD, DDD)
EEE = (EEE, EEE)
GGG = (GGG, GGG)
ZZZ = (ZZZ, ZZZ)";

    const INPUT2: &str = "\
LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)";

    const INPUT3: &str = "\
LR

11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)";

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 2);

        let input_vec: Vec<&str> = INPUT2.split('\n').collect();
        assert_eq!(_part1(&input_vec), 6);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT3.split('\n').collect();
        assert_eq!(_part2(&input_vec), 6);
    }
}

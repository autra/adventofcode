use std::cmp::{max, min};
use std::{cmp::Ordering, fmt::Display};

use regex::Regex;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct SeedRange {
    min: usize,
    max: usize,
}

impl PartialOrd for SeedRange {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.min.cmp(&other.min))
    }
}

impl Ord for SeedRange {
    fn cmp(&self, other: &Self) -> Ordering {
        self.min.cmp(&other.min)
    }
}

impl SeedRange {
    fn from_min_length(min: usize, length: usize) -> SeedRange {
        SeedRange {
            min,
            max: min + length,
        }
    }

    fn contains(&self, val: usize) -> bool {
        val >= self.min && val < self.max
    }

    fn contains_range(&self, val: &Self) -> bool {
        val.min >= self.min && val.max <= self.max
    }

    fn intersects(&self, val: &Self) -> bool {
        return self.min < val.max && self.max > val.min;
    }

    fn add(&self, offset: isize) -> Self {
        Self {
            min: (self.min as isize + offset).try_into().unwrap(),
            max: (self.max as isize + offset).try_into().unwrap(),
        }
    }

    fn split(&self, other: &Self) -> Vec<Self> {
        if other.contains_range(self) {
            Vec::from([self.clone()])
        } else if self.intersects(other) {
            let mut result = Vec::new();
            if self.min < other.min {
                // self.min has a part not included in other at the beginning
                result.push(Self {
                    min: self.min,
                    max: other.min,
                });
            }
            result.push(SeedRange {
                min: max(self.min, other.min),
                max: min(self.max, other.max),
            });
            if self.max > other.max {
                // self has a part outside of other at the end
                result.push(Self {
                    min: other.max,
                    max: self.max,
                });
            }
            result
        } else {
            //disjoint
            Vec::new()
        }
    }
}

// fn combine_ranges(ranges: Vec<SeedRange>) -> Vec<SeedRange> {
//     ranges.sort();
//     for i in 0..ranges.len() {
//         let cur = ranges[i];
//         let next = ranges[i+1]
//     }

// }

fn translate_range(range: SeedRange, current_translation: &[(SeedRange, isize)]) -> Vec<SeedRange> {
    let mut new_ranges = Vec::new();
    for (translation_range, offset) in current_translation {
        if translation_range.contains_range(&range) {
            new_ranges.push(range.add(*offset));
        } else if translation_range.intersects(&range) {
            let mut new_new_ranges =
                translate_ranges(range.split(translation_range), current_translation);
            new_ranges.append(&mut new_new_ranges);
        }
    }
    if new_ranges.len() == 0 {
        new_ranges.push(range)
    }
    new_ranges
}

fn translate_ranges<'a>(
    current_ranges: Vec<SeedRange>,
    current_translation: &[(SeedRange, isize)],
) -> Vec<SeedRange> {
    let mut next_ranges = Vec::new();
    for current_range in current_ranges.iter() {
        let mut new_ranges = translate_range(*current_range, current_translation);
        next_ranges.append(&mut new_ranges);
    }
    // next_ranges = combine_ranges(next_ranges);
    next_ranges
}

fn translate_ids(current_ids: &[usize], current_translation: &[(SeedRange, isize)]) -> Vec<usize> {
    let mut next_ids: Vec<usize> = Vec::new();
    for id in current_ids.iter() {
        let mut found_range = false;
        let mut found_count = 0;
        for (range, offset) in current_translation {
            if range.contains(*id) {
                // if *id >= *source_start && *id < source_start + length {
                next_ids.push((*id as isize + offset).try_into().unwrap());
                found_range = true;
                found_count += 1;
            }
        }
        assert!(found_count <= 1);
        if !found_range {
            next_ids.push(*id);
        }
    }
    next_ids
}

fn parse_translation(lines: &[&str]) -> Vec<Vec<(SeedRange, isize)>> {
    let mut translations = Vec::new();
    let mut current_translation = Vec::new();
    let range_re = Regex::new(r"^(\d+) (\d+) (\d+)").unwrap();
    for line in lines {
        if line == &"" {
            continue;
        } else if let Some((_, [destination_start, source_start, length])) =
            range_re.captures(line).map(|c| c.extract())
        {
            let destination_start: usize = destination_start.parse().unwrap();
            let source_start: usize = source_start.parse().unwrap();
            let length: usize = length.parse().unwrap();
            current_translation.push((
                SeedRange {
                    min: source_start,
                    max: source_start + length,
                },
                destination_start as isize - source_start as isize,
            ));
        } else {
            translations.push(current_translation);
            current_translation = Vec::new();
        }
    }
    // push last translation
    translations.push(current_translation);
    translations
}

fn _part1(lines: &[&str]) -> usize {
    let mut seeds = Vec::new();

    let seed_re = Regex::new(r"(\d+)").unwrap();
    for (_, [seed]) in seed_re.captures_iter(lines[0]).map(|c| c.extract()) {
        seeds.push(seed.parse::<usize>().unwrap());
    }

    let mut current_ids = seeds.clone();

    let translations = parse_translation(&lines[2..]);
    for translation in &translations {
        // starting new sections, do first the translation
        current_ids = translate_ids(&current_ids, &translation);
    }
    assert_eq!(current_ids.len(), seeds.len());
    *current_ids.iter().min().unwrap()
}

fn _part2(lines: &[&str]) -> usize {
    let mut seed_ranges: Vec<SeedRange> = Vec::new();
    let s: Vec<&str> = lines[0].split(" ").skip(1).collect();

    for i in (0..s.len()).step_by(2) {
        let start: usize = s[i].parse().unwrap();
        let length: usize = s[i + 1].parse().unwrap();
        let range = SeedRange::from_min_length(start, length);
        seed_ranges.push(range);
    }

    let translations = parse_translation(&lines[3..]);
    for translation in translations {
        // println!("before {seed_ranges:?}. Applying {translation:?}");
        // starting new sections, do first the translation
        seed_ranges = translate_ranges(seed_ranges, &translation);
        // println!("after {seed_ranges:?}");
    }
    // assert_eq!(current_ids.len(), seeds.len());
    // println!("{seed_ranges:?}");
    seed_ranges.iter().min().unwrap().min
}

#[cfg(test)]
mod tests_seed_range {
    use super::SeedRange;

    #[test]
    fn test_contains() {
        let range = SeedRange { min: 2, max: 10 };
        assert!(range.contains(2));
        assert!(range.contains(9));
        assert!(!range.contains(10));
        assert!(!range.contains(1));
    }

    #[test]
    fn test_contains_range() {
        let range = SeedRange { min: 2, max: 10 };
        assert!(range.contains_range(&SeedRange { min: 2, max: 10 }));
        assert!(range.contains_range(&SeedRange { min: 2, max: 8 }));
        assert!(range.contains_range(&SeedRange { min: 3, max: 8 }));
        assert!(!range.contains_range(&SeedRange { min: 3, max: 11 }));
        assert!(!range.contains_range(&SeedRange { min: 2, max: 11 }));
        assert!(!range.contains_range(&SeedRange { min: 1, max: 10 }));
        assert!(!range.contains_range(&SeedRange { min: 1, max: 11 }));
    }

    #[test]
    fn test_intersects() {
        let range = SeedRange { min: 2, max: 10 };
        assert!(range.intersects(&SeedRange { min: 2, max: 10 }));
        assert!(range.intersects(&SeedRange { min: 0, max: 10 }));
        assert!(range.intersects(&SeedRange { min: 0, max: 3 }));
        assert!(!range.intersects(&SeedRange { min: 0, max: 2 }));
        assert!(range.intersects(&SeedRange { min: 2, max: 3 }));
        assert!(range.intersects(&SeedRange { min: 9, max: 11 }));
        assert!(!range.intersects(&SeedRange { min: 10, max: 13 }));
    }

    #[test]
    fn test_add() {
        let range = SeedRange { min: 2, max: 10 };
        assert_eq!(range.add(2), SeedRange { min: 4, max: 12 });
        assert_eq!(range.add(-1), SeedRange { min: 1, max: 9 });
        assert_eq!(range.add(-2), SeedRange { min: 0, max: 8 });
    }

    #[test]
    fn test_split() {
        let range = SeedRange { min: 2, max: 10 };
        assert_eq!(range.split(&SeedRange { min: 1, max: 11 }), [range]);
        assert_eq!(range.split(&SeedRange { min: 1, max: 10 }), [range]);
        assert_eq!(
            range.split(&SeedRange { min: 1, max: 9 }),
            [SeedRange { min: 2, max: 9 }, SeedRange { min: 9, max: 10 }]
        );
        assert_eq!(range.split(&SeedRange { min: 2, max: 10 }), [range]);
        assert_eq!(
            range.split(&SeedRange { min: 3, max: 10 }),
            [SeedRange { min: 2, max: 3 }, SeedRange { min: 3, max: 10 }]
        );
        assert_eq!(
            range.split(&SeedRange { min: 3, max: 8 }),
            [
                SeedRange { min: 2, max: 3 },
                SeedRange { min: 3, max: 8 },
                SeedRange { min: 8, max: 10 }
            ]
        );
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4";
    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 35);
    }

    #[test]
    fn test_part2_simple() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        // change first line for something more simple
        let mut simple_input = input_vec.clone();
        simple_input[0] = "seeds: 82 1";

        assert_eq!(_part2(&simple_input), 46);

        simple_input[0] = "seeds: 82 2";
        assert_eq!(_part2(&simple_input), 46);

        simple_input[0] = "seeds: 81 2";
        assert_eq!(_part2(&simple_input), 46);

        simple_input[0] = "seeds: 81 1";
        // NOTE: good value ?
        assert_eq!(_part2(&simple_input), 84);
    }

    #[test]
    fn test_part2_one_translation() {
        let input_vec: Vec<&str> = "\
seeds: 79 1

seed-to-soil map:
50 98 2
52 50 48"
            .split('\n')
            .collect();
        assert_eq!(_part2(&input_vec), 81);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part2(&input_vec), 46);
    }
}

use lazy_static::lazy_static;
use std::fmt::Display;
use std::{cell::RefCell, collections::HashMap};

use regex::Regex;

lazy_static! {
    static ref LINE_RE: Regex = Regex::new(r"Card +\d+: ([\d\s]+) \| ([\d\s]+)").unwrap();
}

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn count_winning_numbers(line: &str) -> usize {
    let parts = LINE_RE.captures(line).unwrap();
    let winning_numbers: Vec<usize> = parts[1].split(" ").filter_map(|s| s.parse().ok()).collect();
    let mut count_winning = 0;
    for number in parts[2].split(" ") {
        if let Ok(number) = number.parse() {
            if winning_numbers.contains(&number) {
                count_winning += 1;
            }
        }
    }
    count_winning
}

fn _part1(lines: &[&str]) -> usize {
    let mut acc = 0;
    for line in lines {
        let count_winning = count_winning_numbers(line);
        if count_winning > 0 {
            acc += (2 as usize).pow((count_winning - 1).try_into().unwrap());
        }
    }
    return acc;
}

fn _part2(lines: &[&str]) -> usize {
    let mut cards_count: HashMap<usize, RefCell<usize>> = HashMap::new();
    for i in 0..lines.len() {
        // we start with one copy each
        cards_count.insert(i, RefCell::new(1));
    }
    for (i, line) in lines.iter().enumerate() {
        let count_winning = count_winning_numbers(line);
        // increment each following factored by the current number
        let current_card_count = cards_count.get(&i).unwrap().borrow();
        for j in 1..=count_winning {
            let mut card_count = cards_count.get(&(i + j)).unwrap().borrow_mut();
            *card_count = *card_count + *current_card_count;
        }
    }
    cards_count.values().map(|v| *v.borrow()).sum()
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part1(&input_vec), 13);
    }

    #[test]
    fn test_part2() {
        let input = "\
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part2(&input_vec), 30);
    }
}

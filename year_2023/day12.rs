// this a wip
#![allow(warnings)]
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn line_matches(line: &str, criterias: &[usize]) -> bool {
    let mut it = line.chars();
    let substr_matches = criterias
        .iter()
        .map(|criteria| {
            let mut c = it.next();
            if c == None {
                return *criteria == 0;
            }
            while c != Some('#') {
                c = it.next();
                if c == None {
                    return *criteria == 0;
                }
            }
            // we found a '#'
            let mut count = 0;
            while c == Some('#') {
                count += 1;
                c = it.next();
            }
            // so current char is necessarily either None or '.', so we don't care about it
            *criteria == count
        })
        .all(|c| c);
    // we need to check that none of the remaining chars are '#'
    substr_matches && !it.any(|c| c == '#')
}

fn test_string(line: &[char], mut criterias: Vec<usize>) -> usize {
    // advance the iterator until we find the first interesting char
    let mut i = 0;
    loop {
        if i == line.len() {
            if criterias.len() == 0 || criterias.len() == 1 && criterias[0] == 0 {
                return 1; // match !
            } else {
                return 0;
            }
        }
        if line[i] != '#' && line[i] != '?' {
            if criterias.len() > 0 && criterias[0] == 0 {
                criterias.remove(0);
            }
            println!("advancing");
            i += 1;
        } else {
            break;
        }
    }
    println!("now doing work on {:?} - {criterias:?}", &line[i..]);
    while i < line.len() {
        let c = line[i];
        i += 1;
        println!("{c}");
        match c {
            '#' => {
                // success case
                if criterias.len() > 0 && criterias[0] > 0 {
                    criterias[0] = criterias[0] - 1;
                } else {
                    // we have a # but no criteria : line doesn't match
                    return 0;
                }
            }
            '.' => {
                if criterias[0] == 0 {
                    criterias.remove(0);
                } else {
                    return 0;
                }
            }
            '?' => {
                // copie criterias for second case
                let mut c2 = criterias.clone();
                // case .
                println!("case .");
                if criterias.len() > 0 && criterias[0] == 0 {
                    criterias.remove(0);
                }
                let count1 = test_string(&line[i..], criterias);

                // case #
                println!("case #");
                let mut count2 = 0;
                // no need to test it if we need 0 #, it doesn't match
                if c2.len() > 0 && c2[0] > 0 {
                    c2[0] = c2[0] - 1;
                    count2 = test_string(&line[i..], c2);
                }

                println!("For {line:?} count when . {count1}, count when # {count2}");
                return count1 + count2;
            }
            _ => {
                panic!("This string contains something else than just ., ? and # {c}");
            }
        }
    }
    // end of string
    if criterias.len() == 0 || criterias.len() == 1 && criterias[0] == 0 {
        // this branch matches
        return 1;
    } else {
        return 0;
    }
}

fn _part1(lines: &[&str]) -> usize {
    let mut acc = 0;
    for line in lines {
        let mut sit = line.split(' ');
        let damaged: Vec<char> = sit.next().unwrap().chars().collect();
        let numbers = sit.next().unwrap().split(',');
        let criterias: Vec<usize> = numbers.map(|num| num.parse().unwrap()).collect();
        let count = test_string(&damaged, criterias);
        acc += count;
    }
    acc
}

fn _part2(lines: &[&str]) -> usize {
    0
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    const INPUT: &str = "\
???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1";

    const SIMPLE_INPUT: &str = "\
?###???????? 3,2,1";
    const SIMPLE_INPUT_2: &str = "\
???.### 1,1,3";

    #[test]
    fn test_part1() {
        let input_vec: Vec<&str> = SIMPLE_INPUT_2.split('\n').collect();
        assert_eq!(_part1(&input_vec), 1);
        println!("FOFOOOOOOOOOOOOOOOOOOOOOOOO");
        let input_vec: Vec<&str> = SIMPLE_INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 10);
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part1(&input_vec), 21);
    }

    #[test]
    fn test_part2() {
        let input_vec: Vec<&str> = INPUT.split('\n').collect();
        assert_eq!(_part2(&input_vec), 525152);
    }
}

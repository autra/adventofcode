#!/usr/bin/env python3
import argparse
from http.cookiejar import CookieJar, Cookie
from http.cookies import SimpleCookie
import os
import sqlite3
import urllib


def get_moz_session_cookie(): #cj, ff_cookies):
  with sqlite3.connect('file:/home/augustin/.mozilla/firefox/otbvr2of.default/cookies.sqlite?immutable=1') as con:
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute('''
      SELECT
        name,
        value,
        host,
        path,
        expiry,
        isSecure,
        isHttpOnly
      FROM moz_cookies where host=? and name=?
      ''', ['.adventofcode.com', 'session'])
    return cur.fetchone()


def main(year, day):
  current_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), f'year_{year}', f'day{day}')
  print(f"Creating dir {current_dir}")
  os.mkdir(current_dir)

  # generate load script
  print(f'Generating the 2 loading scripts')
  load_template = '''
drop table if exists day{day};

create table day{day}(line_number integer not null generated always as identity, line text);

\copy day{day}(line) from './input{input_number}'

vacuum analyze day{day};
'''

  with open(os.path.join(current_dir, 'load.sql'), 'w') as load_input:
    load_input.write(load_template.format(day=day, input_number=''))
  with open(os.path.join(current_dir, 'load2.sql'), 'w') as load_example_input:
    load_example_input.write(load_template.format(day=day, input_number='2'))

  # get input
  # input2 can only be manual
  print('Getting cookie from firefox profile')
  cj = CookieJar()
  mozcookie = get_moz_session_cookie()
  cj.set_cookie(Cookie(0,
                       mozcookie['name'],
                       mozcookie['value'],
                       None,
                       False,
                       mozcookie['host'],
                       mozcookie['host'].startswith('.'),
                       mozcookie['host'].startswith('.'),
                       mozcookie['path'],
                       False,
                       mozcookie['isSecure'],
                       mozcookie['expiry'],
                       False,
                       None,
                       None,
                       {})
  )
  print('Downloading input')
  opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
  with opener.open(f"https://adventofcode.com/{year}/day/{day}/input") as r:
    with open(os.path.join(current_dir, 'input'), 'w') as f:
      f.write(r.read().decode('utf-8'))


if __name__=="__main__":
  parser = argparse.ArgumentParser(description='Bootstrap one day')
  parser.add_argument('year', type=int,
                      help='The year of the solution folder to generate')
  parser.add_argument('day', type=int,
                      help='The day of the solution folder to generate')

  args = parser.parse_args()
  main(args.year, args.day)

use regex::Regex;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

struct Order {
    what: usize,
    from: usize,
    to: usize,
}

fn parse(lines: &[&str]) -> (Vec<Order>, Vec<Vec<char>>) {
    let num_stack_re = Regex::new(r"^(?:\s*\d\s*)+(\d)\s*$").unwrap();
    let order_re = Regex::new(r"^move (\d+) from (\d) to (\d)$").unwrap();

    let mut num_stack = 0;
    let mut orders: Vec<Order> = Vec::new();
    let mut stack_lines: Vec<&str> = Vec::new();
    // read in reverse to get the num of stack *before* getting the stack
    for line in lines.iter().rev() {
        if num_stack_re.is_match(line) {
            num_stack = num_stack_re
                .captures(line)
                .unwrap()
                .get(1)
                .unwrap()
                .as_str()
                .parse::<usize>()
                .unwrap();
        } else if order_re.is_match(line) {
            let cap = order_re.captures(line).unwrap();
            orders.push(Order {
                what: cap.get(1).unwrap().as_str().parse::<usize>().unwrap(),
                from: cap.get(2).unwrap().as_str().parse::<usize>().unwrap(),
                to: cap.get(3).unwrap().as_str().parse::<usize>().unwrap(),
            });
        } else if line != &"" {
            // this is the stack
            stack_lines.push(line);
        }
    }

    let mut stack_re_str = String::from(r"^");
    let mut parsed_stack: Vec<Vec<char>> = Vec::new();
    for _i in 0..num_stack {
        stack_re_str += r"(?:\[([A-Z])\](?:\s|$)|\s{4})?";
        parsed_stack.push(Vec::new());
    }
    stack_re_str += r"$";
    let stack_re = Regex::new(&stack_re_str).unwrap();
    // parse stack
    for line in stack_lines.iter() {
        let caps = stack_re.captures(line).unwrap();
        for i in 1..caps.len() {
            match caps.get(i) {
                Some(s) => parsed_stack[i - 1].push(s.as_str().chars().next().unwrap()),
                None => {}
            }
        }
    }
    (orders, parsed_stack)
}

fn build_result(parsed_stack: Vec<Vec<char>>) -> String {
    let mut acc = String::from("");
    for mut stack in parsed_stack {
        match stack.pop() {
            Some(c) => acc += &c.to_string(),
            None => {}
        };
    }
    acc
}

fn _part1(lines: &[&str]) -> String {
    let (orders, mut parsed_stack) = parse(lines);
    // don't forget to read orders in reverse
    for order in orders.iter().rev() {
        // apply orders
        for _i in 0..order.what {
            let val = parsed_stack[order.from - 1].pop().unwrap();
            parsed_stack[order.to - 1].push(val);
        }
    }
    build_result(parsed_stack)
}

fn _part2(lines: &[&str]) -> String {
    let (orders, mut parsed_stack) = parse(lines);
    // don't forget to read orders in reverse
    for order in orders.iter().rev() {
        // apply orders
        let mut crane = Vec::new();
        for _i in 0..order.what {
            crane.push(parsed_stack[order.from - 1].pop().unwrap());
        }
        for elem in crane.iter().rev() {
            parsed_stack[order.to - 1].push(elem.to_owned());
        }
    }
    build_result(parsed_stack)
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = r"    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), "CMZ");
    }

    #[test]
    fn test_part2() {
        let input = r"    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part2(&input_vec), "MCD");
    }
}

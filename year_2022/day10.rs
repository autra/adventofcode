use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    let result = format!("\n{}", _part2(lines));
    Box::new(result)
}

fn _part1(lines: &[&str]) -> i32 {
    let mut lines_it = lines.iter();
    let mut cycle = 0;
    let mut in_add = false;
    let mut add_val: i32 = 0;
    let mut acc: i32 = 1;
    let mut result = 0;
    loop {
        cycle += 1;
        // Starting new cycle
        // is this a cycle of interest? (-20 mod 40)
        if (cycle - 20) % 40 == 0 {
            // aggregating the signal length
            result += acc * cycle;
        }
        // are we adding
        if in_add {
            // next time we are not :-)
            in_add = false;
            // add
            acc += add_val;
            // and do nothing else
            continue;
        }
        // read next line
        let curr_line = match lines_it.next() {
            Some(l) => l,
            // end of input
            None => break,
        };
        if curr_line == &"noop" {
            continue;
        } else {
            in_add = true;
            let mut it = curr_line.split(' ');
            it.next(); // skip addx

            // add this value next cycle
            add_val = it.next().unwrap().parse::<i32>().unwrap();
        }
    }
    result
}

fn _part2(lines: &[&str]) -> String {
    let mut lines_it = lines.iter();
    let mut cycle = 0;
    let mut in_add = false;
    let mut add_val: i32 = 0;
    let mut acc: i32 = 1;
    let mut result = String::new();
    loop {
        cycle += 1;
        let mut curr_line = "";
        if !in_add {
            // read next line
            curr_line = match lines_it.next() {
                Some(l) => l,
                // end of input
                None => break,
            };
        }
        // Starting new cycle
        // What should we display?
        // note position = cycle - 1
        if ((cycle - 1) % 40 - acc).abs() <= 1 {
            result += "■";
        } else {
            result += " ";
        }
        // is this a cycle of interest? (-20 mod 40)
        if cycle % 40 == 0 {
            // aggregating the signal length
            result += "\n";
        }

        // are we adding
        if in_add {
            // next time we are not :-)
            in_add = false;
            // add
            acc += add_val;
            // and do nothing else
        } else if curr_line != "noop" {
            in_add = true;
            let mut it = curr_line.split(' ');
            it.next(); // skip addx

            // add this value next cycle
            add_val = it.next().unwrap().parse::<i32>().unwrap();
        }
    }
    result
}

#[allow(dead_code)]
pub fn main() {
    let input = "\
noop
addx 3
addx -5";
    let input_vec: Vec<&str> = input.split('\n').collect();
    _part1(&input_vec);
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), 13140);
    }

    #[test]
    fn test_part2() {
        let input = "\
addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop";
        let input_vec: Vec<&str> = input.split('\n').collect();
        let result = "\
■■  ■■  ■■  ■■  ■■  ■■  ■■  ■■  ■■  ■■  
■■■   ■■■   ■■■   ■■■   ■■■   ■■■   ■■■ 
■■■■    ■■■■    ■■■■    ■■■■    ■■■■    
■■■■■     ■■■■■     ■■■■■     ■■■■■     
■■■■■■      ■■■■■■      ■■■■■■      ■■■■
■■■■■■■       ■■■■■■■       ■■■■■■■     
";
        assert_eq!(_part2(&input_vec), result);
    }
}

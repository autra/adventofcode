use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn parse(lines: &[&str]) -> (Vec<Vec<u32>>, usize, usize) {
    let mut map: Vec<Vec<u32>> = lines
        .iter()
        .map(|l| l.chars().map(|c| c as u32).collect())
        .collect();

    // find start position
    let max_x = map[0].len();
    let max_y = map.len();
    let mut start_x = max_x;
    let mut start_y = max_y;
    'outer1: for (y, line) in map.iter().enumerate() {
        for (x, c) in line.iter().enumerate() {
            if c == &('S' as u32) {
                start_x = x;
                start_y = y;
                break 'outer1;
            }
        }
    }
    map[start_y][start_x] = 'a' as u32;
    if start_x == max_x || start_y == max_y {
        panic!("S not found!!");
    }
    (map, start_x, start_y)
}

fn hike(map: Vec<Vec<u32>>, starting_points: &[(usize, usize)]) -> usize {
    let mut visited = Vec::from(starting_points);
    let mut last_visited: Vec<(usize, usize)> = Vec::from(starting_points);
    let mut step = 0;
    let max_x = map[0].len();
    let max_y = map.len();
    'outer: loop {
        step += 1;
        let current_visited = last_visited.clone();
        last_visited = Vec::new();
        for current in current_visited {
            let current_val = map[current.1][current.0] as i32;
            for (deltax, deltay) in [(1, 0), (0, 1), (-1, 0), (0, -1)] {
                let y: i32 = current.1 as i32;
                let x: i32 = current.0 as i32;
                if y + deltay < 0 || y + deltay >= max_y as i32 {
                    continue;
                }
                if x + deltax < 0 || x + deltax >= max_x as i32 {
                    continue;
                }
                let next_x = usize::try_from(x + deltax).unwrap();
                let next_y = usize::try_from(y + deltay).unwrap();
                if visited.contains(&(next_x, next_y)) {
                    continue;
                }
                let next_char = map[next_y][next_x] as i32;
                let mut next_val = next_char;
                if next_char == 'E' as i32 {
                    next_val = 'z' as i32;
                }
                if next_val - current_val <= 1 {
                    if next_char == 'E' as i32 {
                        break 'outer;
                    }
                    visited.push((next_x, next_y));
                    last_visited.push((next_x, next_y));
                }
            }
        }
    }

    step
}

fn _part1(lines: &[&str]) -> usize {
    let (map, start_x, start_y) = parse(lines);
    hike(map, &[(start_x, start_y)])
}

fn _part2(lines: &[&str]) -> usize {
    let (map, _, _) = parse(lines);
    let mut starting_points: Vec<(usize, usize)> = Vec::new();
    for (y, line) in map.iter().enumerate() {
        for (x, char) in line.iter().enumerate() {
            if *char == 'a' as u32 {
                starting_points.push((x, y));
            }
        }
    }
    hike(map, starting_points.as_slice())
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi";
        let input_vec: Vec<&str> = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part1(&input_vec), 31);
    }

    #[test]
    fn test_part2() {
        let input = "\
Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi";
        let input_vec: Vec<&str> = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part2(&input_vec), 29);
    }
}

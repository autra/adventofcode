use regex::Regex;
use std::cmp::max;
use std::cmp::min;
use std::collections::HashSet;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines, 2000000))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines, 4000000))
}

struct Sensor {
    x: i64,
    y: i64,
    distance: u64,
}

fn calculate_m_distance(x1: i64, y1: i64, x2: i64, y2: i64) -> u64 {
    return ((x1 - x2).abs() + (y1 - y2).abs()) as u64;
}

fn parse(lines: &[&str]) -> (Vec<Sensor>, Vec<(i64, i64)>, i64, i64, i64, i64) {
    let mut sensors: Vec<Sensor> = Vec::new();
    let mut beacons: Vec<(i64, i64)> = Vec::new();
    let sensor_re =
        Regex::new(r"Sensor at x=(-?\d*), y=(-?\d*): closest beacon is at x=(-?\d*), y=(-?\d*)")
            .unwrap();
    // up this value, it's useless to check
    // typically it's max(x+distance) for each sensors.
    let mut max_x = 0;
    let mut min_x = 0;
    let mut max_y = 0;
    let mut min_y = 0;

    for line in lines {
        let x = sensor_re
            .captures(line)
            .unwrap()
            .get(1)
            .unwrap()
            .as_str()
            .parse()
            .unwrap();
        let y = sensor_re
            .captures(line)
            .unwrap()
            .get(2)
            .unwrap()
            .as_str()
            .parse()
            .unwrap();
        let x_beacon = sensor_re
            .captures(line)
            .unwrap()
            .get(3)
            .unwrap()
            .as_str()
            .parse()
            .unwrap();
        let y_beacon = sensor_re
            .captures(line)
            .unwrap()
            .get(4)
            .unwrap()
            .as_str()
            .parse()
            .unwrap();

        let distance = calculate_m_distance(x, y, x_beacon, y_beacon);
        sensors.push(Sensor {
            x,
            y,
            distance: distance.try_into().unwrap(),
        });

        beacons.push((x_beacon, y_beacon));

        min_x = min(min_x, x - distance as i64);
        max_x = max(max_x, x + distance as i64);
        min_y = min(min_y, y - distance as i64);
        max_y = max(max_y, y + distance as i64);
    }

    return (sensors, beacons, min_x, max_x, min_y, max_y);
}

fn test_row(
    row: u64,
    sensors: &[Sensor],
    beacons: &[(i64, i64)],
    min_x: i64,
    max_x: i64,
) -> (u64, HashSet<i64>) {
    let mut beacons_on_tested_row: HashSet<i64> = HashSet::new();
    for beacon in beacons {
        if beacon.1 == (row as i64) {
            beacons_on_tested_row.insert(beacon.0);
        }
    }

    let mut impossible_pos: u64 = 0;
    for x in min_x..max_x + 1 {
        for sensor in sensors {
            let distance = calculate_m_distance(x, row as i64, sensor.x, sensor.y);
            if distance <= sensor.distance {
                impossible_pos += 1;
                break;
            }
        }
    }
    return (impossible_pos, beacons_on_tested_row);
}

fn _part1(lines: &[&str], tested_row: u64) -> u64 {
    let (sensors, beacons, min_x, max_x, _, _) = parse(lines);
    let (impossible_pos, beacons_on_tested_row) =
        test_row(tested_row, &sensors, &beacons, min_x, max_x);
    return impossible_pos - beacons_on_tested_row.len() as u64;
}

fn test_point(sensors: &[Sensor], beacons: &[(i64, i64)], x: i64, y: i64) -> Option<(i64, i64)> {
    for beacon in beacons {
        if beacon.0 == x && beacon.1 == y {
            return None;
        }
    }
    for sensor in sensors {
        let distance = calculate_m_distance(x, y, sensor.x, sensor.y);
        if distance <= sensor.distance {
            return None;
        }
    }
    return Some((x, y));
}

fn _part2(lines: &[&str], max_window: i64) -> i64 {
    let (sensors, beacons, min_x, max_x, min_y, max_y) = parse(lines);

    println!(
        "minx {} maxx {} miny {} maxy {}",
        min_x, max_x, min_y, max_y
    );

    let min_x = max(min_x, 0);
    let max_x = min(max_x, max_window);
    let min_y = max(min_y, 0);
    let max_y = min(max_y, max_window);
    println!(
        "minx {} maxx {} miny {} maxy {}",
        min_x, max_x, min_y, max_y
    );

    // if we have only one position, it means we are on the edge of a beacon "area"
    // so we iterate only on those points instead of iterating on the whole space of max_window * max_window, which is huge
    for sensor in &sensors {
        // iterate over the edge + 1
        let mut x = sensor.x + sensor.distance as i64 + 1;
        let mut y = sensor.y;
        // top right edge
        while y != sensor.y + sensor.distance as i64 + 1 {
            // check bounds
            if x >= min_x && y >= min_y && x <= max_x && y <= max_y {
                match test_point(&sensors, &beacons, x, y) {
                    Some(p) => return p.0 * 4000000 + p.1,
                    None => {}
                }
            }

            // go to top left
            x -= 1;
            y += 1
        }
        // top left edge
        while x != sensor.x - sensor.distance as i64 - 1 {
            // check bounds
            if x >= min_x && y >= min_y && x <= max_x && y <= max_y {
                match test_point(&sensors, &beacons, x, y) {
                    Some(p) => return p.0 * 4000000 + p.1,
                    None => {}
                }
            }
            x -= 1;
            y -= 1;
        }
        // bottom left edge
        while y != sensor.y - sensor.distance as i64 - 1 {
            // check bounds
            if x >= min_x && y >= min_y && x <= max_x && y <= max_y {
                match test_point(&sensors, &beacons, x, y) {
                    Some(p) => return p.0 * 4000000 + p.1,
                    None => {}
                }
            }
            x += 1;
            y -= 1;
        }
        // bottom right edge
        while x != sensor.x + sensor.distance as i64 + 1 {
            // check bounds
            if x >= min_x && y >= min_y && x <= max_x && y <= max_y {
                match test_point(&sensors, &beacons, x, y) {
                    Some(p) => return p.0 * 4000000 + p.1,
                    None => {}
                }
            }
            x += 1;
            y += 1;
        }
    }
    return 0;
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part1(&input_vec, 10), 26);
    }

    #[test]
    fn test_part2() {
        let input = "\
Sensor at x=2, y=18: closest beacon is at x=-2, y=15
Sensor at x=9, y=16: closest beacon is at x=10, y=16
Sensor at x=13, y=2: closest beacon is at x=15, y=3
Sensor at x=12, y=14: closest beacon is at x=10, y=16
Sensor at x=10, y=20: closest beacon is at x=10, y=16
Sensor at x=14, y=17: closest beacon is at x=10, y=16
Sensor at x=8, y=7: closest beacon is at x=2, y=10
Sensor at x=2, y=0: closest beacon is at x=2, y=10
Sensor at x=0, y=11: closest beacon is at x=2, y=10
Sensor at x=20, y=14: closest beacon is at x=25, y=17
Sensor at x=17, y=20: closest beacon is at x=21, y=22
Sensor at x=16, y=7: closest beacon is at x=15, y=3
Sensor at x=14, y=3: closest beacon is at x=15, y=3
Sensor at x=20, y=1: closest beacon is at x=15, y=3";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part2(&input_vec, 20), 56000011);
    }
}

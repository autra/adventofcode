use std::cmp::Ordering;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn tokenize(array: &str) -> Vec<String> {
    let mut acc: Vec<String> = Vec::new();
    if array.is_empty() {
        return acc;
    }
    let mut current_val = String::new();
    let mut subarray_level = 0;
    let mut it = array.chars();
    loop {
        match it.next() {
            Some(',') => {
                if subarray_level == 0 {
                    acc.push(current_val);
                    current_val = String::new();
                } else {
                    current_val += ",";
                }
            }
            Some('[') => {
                subarray_level += 1;
                current_val += "[";
            }
            Some(']') => {
                subarray_level -= 1;
                current_val += "]";
            }
            Some(v) => current_val += &v.to_string(),
            None => {
                acc.push(current_val);
                return acc;
            }
        }
    }
}

fn compare(left: &str, right: &str) -> Ordering {
    let is_left_array = left.starts_with("[") && left.ends_with("]");
    let is_right_array = right.starts_with("[") && right.ends_with("]");
    if is_left_array && is_right_array {
        // get arrays without the delimiters
        let left_tokens = tokenize(&left[1..left.len() - 1]);
        let right_tokens = tokenize(&right[1..right.len() - 1]);
        let mut lit = left_tokens.iter();
        let mut rit = right_tokens.iter();

        loop {
            let leftval = match lit.next() {
                Some(v) => v,
                None => break,
            };
            let rightval = match rit.next() {
                Some(v) => v,
                None => return Ordering::Greater,
            };
            let cmp = compare(leftval, rightval);
            match cmp {
                Ordering::Equal => continue,
                other_val => return other_val,
            };
        }
        // is there any other val in right?
        match rit.next() {
            // right has still some inputs, but left is done
            Some(_) => return Ordering::Less,
            // right doesn't have input as well, and all are equals
            None => return Ordering::Equal,
        }
    } else if is_left_array {
        // right is not, let's make is so and compare
        let right = "[".to_owned() + right + "]";
        return compare(left, &right);
    } else if is_right_array {
        let left = "[".to_owned() + left + "]";
        return compare(&left, right);
    } else {
        // finally parse integers \o/
        let right: u32 = right.parse().unwrap();
        let left: u32 = left.parse().unwrap();
        return left.cmp(&right);
    }
}

fn _part1(lines: &[&str]) -> usize {
    let mut acc = 0;
    let pairs: Vec<&&str> = lines.iter().filter(|l| !l.is_empty()).collect();
    for i in (0..pairs.len()).step_by(2) {
        acc = match compare(pairs[i], pairs[i + 1]) {
            Ordering::Less => acc + i / 2 + 1,
            Ordering::Greater => acc,
            Ordering::Equal => panic!("Left and right are equal"),
        };
    }
    return acc;
}

fn _part2(lines: &[&str]) -> usize {
    let mut pairs: Vec<&str> = lines.iter().filter(|l| !l.is_empty()).map(|&v| v).collect();
    pairs.push("[[2]]");
    pairs.push("[[6]]");
    pairs.sort_by(|a, b| compare(a, b));
    (pairs.iter().position(|e| *e == "[[2]]").unwrap() + 1)
        * (pairs.iter().position(|e| *e == "[[6]]").unwrap() + 1)
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_tokenize() {
        let vec: &[&str] = &[];
        assert_eq!(tokenize(""), vec);
        assert_eq!(tokenize("1"), &["1"]);
        assert_eq!(tokenize("[1]"), &["[1]"]);
        assert_eq!(tokenize("1,2,3,4"), &["1", "2", "3", "4"]);
        assert_eq!(tokenize("[1],[2,3,4]"), &["[1]", "[2,3,4]"]);
    }

    #[test]
    fn test_part1() {
        let input = "\
[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part1(&input_vec), 13);
    }

    #[test]
    fn test_part2() {
        let input = "\
[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part2(&input_vec), 140);
    }
}

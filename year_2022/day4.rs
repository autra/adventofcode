use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> usize {
    return lines
        .iter()
        .map(|l| l.split(',').collect())
        .map(
            // s1 is one line
            |s1: Vec<&str>| {
                s1.iter()
                    .map(
                        // part is one half of line
                        |part| {
                            part.split('-')
                                .map(|number| number.parse::<i32>().unwrap())
                                .collect::<Vec<i32>>()
                        },
                    )
                    .collect::<Vec<Vec<i32>>>()
            },
        )
        .filter(|v| {
            (v[0][0] <= v[1][0] && v[0][1] >= v[1][1]) || (v[1][0] <= v[0][0] && v[1][1] >= v[0][1])
        })
        .count();
}

fn _part2(lines: &[&str]) -> usize {
    return lines
        .iter()
        .map(|l| l.split(',').collect())
        .map(
            // s1 is one line
            |s1: Vec<&str>| {
                s1.iter()
                    .map(
                        // part is one half of line
                        |part| {
                            part.split('-')
                                .map(|number| number.parse::<i32>().unwrap())
                                .collect::<Vec<i32>>()
                        },
                    )
                    .collect::<Vec<Vec<i32>>>()
            },
        )
        .filter(|v| {
            (v[0][0] <= v[1][1] && v[0][1] >= v[1][1]) || (v[1][0] <= v[0][1] && v[1][1] >= v[0][1])
        })
        .count();
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let mut input = "\
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";
        let mut input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), 2);

        input = "3-76,10-98";
        input_vec = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), 0);
    }

    #[test]
    fn test_part2() {
        let input = "\
2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part2(&input_vec), 4);
    }
}

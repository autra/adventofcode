use std::collections::HashMap;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> i32 {
    let val_map: HashMap<&str, i32> =
        HashMap::from([("A", 1), ("B", 2), ("C", 3), ("X", 1), ("Y", 2), ("Z", 3)]);

    let mut score = 0;
    for line in lines {
        let test: Vec<&str> = line.split(' ').collect();

        let other_val = val_map[test[0]];
        let my_val = val_map[test[1]];

        let result = my_val - other_val;
        if result == 0 {
            // draw
            score = score + my_val + 3;
        } else if result == 1 || result == -2 {
            // win
            score = score + my_val + 6;
        } else if result == -1 || result == 2 {
            // loss
            score += my_val;
        } else {
            // wat?
            panic!("err")
        }
    }
    score
}

fn _part2(lines: &[&str]) -> i32 {
    let val_map: HashMap<&str, i32> = HashMap::from([("A", 1), ("B", 2), ("C", 3)]);

    let mut score = 0;
    for line in lines {
        let test: Vec<&str> = line.split(' ').collect();

        let other_val = val_map[test[0]];
        let outcome = test[1];

        if outcome == "X" {
            // need to lose
            score += ((other_val + 1) % 3) + 1;
        } else if outcome == "Y" {
            // draw
            score += 3 + other_val;
        } else if outcome == "Z" {
            // win
            score += 6 + (other_val % 3) + 1;
        } else {
            panic!("err")
        }
    }
    score
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
A Y
B X
C Z";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), 15);
    }

    #[test]
    fn test_part2() {
        let input = "\
A Y
B X
C Z";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part2(&input_vec), 12);
    }
}

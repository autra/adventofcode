use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn parse(lines: &[&str]) -> Vec<Vec<u32>> {
    return lines
        .iter()
        .filter(|line_arr| *line_arr != &"") // remove last empty line
        .map(|l| l.chars().map(|c| c.to_digit(10).unwrap()).collect())
        .collect();
}

fn _part1(lines: &[&str]) -> usize {
    let grid = parse(lines);
    let mut nb_visible = 4 * (grid.len() - 1); // all the sides
                                               // now let's inspect the others
    let xmax = grid[0].len();
    let ymax = grid.len();
    for y in 1..ymax - 1 {
        for x in 1..xmax - 1 {
            // this assumes every lines has the same length
            // inspect horizontal
            let current_tree = grid[y][x];
            if current_tree > *grid[y][0..x].iter().max().unwrap()
                || current_tree > *grid[y][x + 1..xmax].iter().max().unwrap()
                || current_tree > grid[0..y].iter().map(|v| v[x]).max().unwrap()
                || current_tree > grid[y + 1..ymax].iter().map(|v| v[x]).max().unwrap()
            {
                nb_visible += 1;
            }
        }
    }
    nb_visible
}

fn _part2(lines: &[&str]) -> usize {
    let grid = parse(lines);
    let xmax = grid[0].len();
    let ymax = grid.len();
    let mut max_scenic_score = 0;
    // note we skip edges, because their score will be 0 anyway
    for y in 1..ymax - 1 {
        for x in 1..xmax - 1 {
            // this assumes every lines has the same length
            // inspect horizontal
            let current_tree = grid[y][x];
            let horizontal_before_scenic_score = match grid[y][0..x]
                .iter()
                .rev()
                .position(|elem| *elem >= current_tree)
            {
                Some(pos) => pos + 1,
                None => grid[y][0..x].len(),
            };
            let horizontal_after_scenic_score = match grid[y][x + 1..xmax]
                .iter()
                .position(|elem| *elem >= current_tree)
            {
                Some(pos) => pos + 1,
                None => grid[y][x + 1..xmax].len(),
            };
            let vertical_before = grid[0..y].iter().map(|v| v[x]);
            let vblen = vertical_before.len();
            let vertical_before_scenic_score =
                match vertical_before.rev().position(|elem| elem >= current_tree) {
                    Some(pos) => pos + 1,
                    None => vblen,
                };
            let mut vertical_after = grid[y + 1..ymax].iter().map(|v| v[x]);
            let valen = vertical_after.len();
            let vertical_after_scenic_score =
                match vertical_after.position(|elem| elem >= current_tree) {
                    Some(pos) => pos + 1,
                    None => valen,
                };
            max_scenic_score = std::cmp::max(
                max_scenic_score,
                horizontal_before_scenic_score
                    * horizontal_after_scenic_score
                    * vertical_before_scenic_score
                    * vertical_after_scenic_score,
            );
        }
    }
    max_scenic_score
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
30373
25512
65332
33549
35390
";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), 21);
    }

    #[test]
    fn test_part2() {
        let input = "\
30373
25512
65332
33549
35390
";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part2(&input_vec), 8);
    }
}

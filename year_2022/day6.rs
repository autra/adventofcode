use std::collections::HashSet;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines[0]))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines[0]))
}

fn get_message_start(input: &str, seq_len: usize) -> usize {
    for i in seq_len..input.len() + 1 {
        let slice: HashSet<char> = input[i - seq_len..i].chars().collect();
        if slice.len() == seq_len {
            return i;
        }
    }
    0
}

fn _part1(input: &str) -> usize {
    get_message_start(input, 4)
}

fn _part2(input: &str) -> usize {
    get_message_start(input, 14)
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        assert_eq!(_part1("bvwbjplbgvbhsrlpgdmjqwftvncz"), 5);
        assert_eq!(_part1("nppdvjthqldpwncqszvftbrmjlhg"), 6);
        assert_eq!(_part1("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"), 10);
        assert_eq!(_part1("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), 11);
    }

    #[test]
    fn test_part2() {
        assert_eq!(_part2("mjqjpqmgbljsphdztnvjfqwrcgsmlb"), 19);
        assert_eq!(_part2("bvwbjplbgvbhsrlpgdmjqwftvncz"), 23);
        assert_eq!(_part2("nppdvjthqldpwncqszvftbrmjlhg"), 23);
        assert_eq!(_part2("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"), 29);
        assert_eq!(_part2("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), 26);
    }
}

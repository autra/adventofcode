use regex::Regex;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

struct Monkey {
    items: Vec<u64>,
    operand: String,
    operation: String,
    divisible_test: u64,
    monkey_true: u64,
    monkey_false: u64,
    nb_inspection: u64,
}

impl Monkey {
    fn inspect(&self, item: u64) -> u64 {
        let operand1 = item;
        let operand2: u64 = if self.operand == "old" {
            item
        } else {
            self.operand.parse().unwrap()
        };
        if self.operation == "+" {
            operand1 + operand2
        } else {
            operand1 * operand2
        }
    }

    fn bored_by(&self, item: u64) -> u64 {
        item / 3
    }

    fn throw(&self, item: u64) -> u64 {
        if item % self.divisible_test == 0 {
            self.monkey_true
        } else {
            self.monkey_false
        }
    }
}

// Monkey 0:
// Starting items: 79, 98
// Operation: new = old * 19
// Test: divisible by 23
// If true: throw to monkey 2
// If false: throw to monkey 3
fn parse(lines: &[&str]) -> (Vec<Monkey>, u64) {
    let mut modulo = 1;
    let mut monkeys: Vec<Monkey> = Vec::new();
    let monkey_regex = Regex::new(r"Monkey (\d+):").unwrap();
    let starting_regex = Regex::new(r"Starting items: (.*)").unwrap();
    let operation_regex = Regex::new(r"new = old (\+|\*) ([^ ]*)").unwrap();
    let test_regex = Regex::new(r"Test: divisible by (\d+)").unwrap();
    let true_regex = Regex::new(r"If true: throw to monkey (\d+)").unwrap();
    let false_regex = Regex::new(r"If false: throw to monkey (\d+)").unwrap();
    let mut it = lines.iter();
    while let Some(line) = it.next() {
        if !monkey_regex.is_match(line) {
            panic!("Should match first line!");
        }
        let line = it.next().unwrap();
        let starting_items: Vec<u64> = starting_regex
            .captures(line)
            .unwrap()
            .get(1)
            .unwrap()
            .as_str()
            .split(", ")
            .map(|s| s.to_string().parse().unwrap())
            .collect();
        let line = it.next().unwrap();
        let operation = operation_regex
            .captures(line)
            .unwrap()
            .get(1)
            .unwrap()
            .as_str()
            .to_string();
        let operand = operation_regex
            .captures(line)
            .unwrap()
            .get(2)
            .unwrap()
            .as_str()
            .to_string();
        let line = it.next().unwrap();
        let divisible_test = test_regex
            .captures(line)
            .unwrap()
            .get(1)
            .unwrap()
            .as_str()
            .parse()
            .unwrap();
        let line = it.next().unwrap();
        let monkey_true = true_regex
            .captures(line)
            .unwrap()
            .get(1)
            .unwrap()
            .as_str()
            .parse()
            .unwrap();
        let line = it.next().unwrap();
        let monkey_false = false_regex
            .captures(line)
            .unwrap()
            .get(1)
            .unwrap()
            .as_str()
            .parse()
            .unwrap();
        // skip empty line
        it.next();
        modulo *= divisible_test;
        monkeys.push(Monkey {
            items: starting_items,
            operand,
            operation,
            divisible_test,
            monkey_true,
            monkey_false,
            nb_inspection: 0,
        });
    }
    (monkeys, modulo)
}

fn reticulate_monkeys(
    mut monkeys: Vec<Monkey>,
    can_be_bored: bool,
    modulo: u64,
    nb_round: u64,
) -> u64 {
    for _ in 0..nb_round {
        for i in 0..monkeys.len() {
            for j in 0..monkeys[i].items.len() {
                let mut item = monkeys[i].inspect(monkeys[i].items[j]);
                monkeys[i].nb_inspection += 1;
                if can_be_bored {
                    item = monkeys[i].bored_by(item);
                }
                item %= modulo;
                let monkey_idx = monkeys[i].throw(item);
                monkeys[monkey_idx as usize].items.push(item);
            }
            monkeys[i].items = Vec::new();
        }
    }
    monkeys.sort_by(|a, b| b.nb_inspection.partial_cmp(&a.nb_inspection).unwrap());
    monkeys[0].nb_inspection * monkeys[1].nb_inspection
}

fn _part1(lines: &[&str]) -> u64 {
    let (monkeys, modulo) = parse(lines);
    reticulate_monkeys(monkeys, true, modulo, 20)
}

fn _part2(lines: &[&str]) -> u64 {
    let (monkeys, modulo) = parse(lines);
    reticulate_monkeys(monkeys, false, modulo, 10000)
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), 10605);
    }

    #[test]
    fn test_part2() {
        let input = "\
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part2(&input_vec), 2713310158);
    }
}

use std::collections::HashSet;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}

pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

#[derive(Copy, Clone)]
struct Point {
    x: i32,
    y: i32,
}

struct TheMap {
    taken: HashSet<(i32, i32)>,
    floor: i32,
}

impl TheMap {
    fn can_fall_to(&self, pos: &(i32, i32)) -> bool {
        return !self.taken.contains(pos) && self.floor > pos.1;
    }

    fn outside(&self, pos: &(i32, i32)) -> bool {
        return self.floor - 1 <= pos.1;
    }
}

fn parse(input_lines: &[&str]) -> TheMap {
    let mut max_y = 0;
    let mut taken: HashSet<(i32, i32)> = HashSet::new();
    for line in input_lines {
        let mut start: Option<Point> = None;
        for coordinates in line.split(" -> ") {
            let mut current = coordinates.split(',');
            let current_pts = Point {
                x: current.next().unwrap().parse().unwrap(),
                y: current.next().unwrap().parse().unwrap(),
            };
            max_y = std::cmp::max(max_y, current_pts.y);
            if let Some(p) = start {
                let s;
                let end;
                if p.x == current_pts.x && p.y <= current_pts.y
                    || p.y == current_pts.y && p.x <= current_pts.x
                {
                    s = p;
                    end = current_pts
                } else {
                    s = current_pts;
                    end = p;
                }
                for i in 0..(end.x - s.x).abs() + (end.y - s.y).abs() + 1 {
                    taken.insert((
                        s.x + i * (end.x - s.x).signum(),
                        s.y + i * (end.y - s.y).signum(),
                    ));
                }
            }
            start = Some(current_pts);
        }
    }
    // the floor
    return TheMap {
        taken,
        floor: max_y + 2,
    };
}

fn _part1(lines: &[&str]) -> usize {
    let mut map = parse(lines);
    let mut count = 0;
    'outer: loop {
        let mut grain = Point { x: 500, y: 0 };
        loop {
            if map.can_fall_to(&(grain.x, grain.y + 1)) {
                grain.y += 1;
            } else if map.can_fall_to(&(grain.x - 1, grain.y + 1)) {
                grain.y += 1;
                grain.x -= 1;
            } else if map.can_fall_to(&(grain.x + 1, grain.y + 1)) {
                grain.y += 1;
                grain.x += 1;
            } else {
                // cannot fall any more
                break;
            }

            // is already outside
            if map.outside(&(grain.x, grain.y)) {
                break 'outer;
            }
        }
        map.taken.insert((grain.x, grain.y));
        count += 1;
    }
    return count;
}

fn _part2(lines: &[&str]) -> usize {
    let mut map = parse(lines);
    let mut count = 0;
    'outer: loop {
        let mut grain = Point { x: 500, y: 0 };
        loop {
            if map.can_fall_to(&(grain.x, grain.y + 1)) {
                grain.y += 1;
            } else if map.can_fall_to(&(grain.x - 1, grain.y + 1)) {
                grain.y += 1;
                grain.x -= 1;
            } else if map.can_fall_to(&(grain.x + 1, grain.y + 1)) {
                grain.y += 1;
                grain.x += 1;
            } else {
                // cannot fall any more
                count += 1;
                if grain.x == 500 && grain.y == 0 {
                    // we've blocked the source!
                    break 'outer;
                }
                map.taken.insert((grain.x, grain.y));
                break;
            }
        }
    }
    return count;
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part1(&input_vec), 24);
    }

    #[test]
    fn test_part2() {
        let input = "\
498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9";
        let input_vec = input.split('\n').collect::<Vec<&str>>();
        assert_eq!(_part2(&input_vec), 93);
    }
}

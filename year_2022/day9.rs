use std::collections::HashSet;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn calculate_distance(pos1: (i32, i32), pos2: (i32, i32)) -> i32 {
    std::cmp::max((pos1.0 - pos2.0).abs(), (pos1.1 - pos2.1).abs())
}

fn _part1(lines: &[&str]) -> usize {
    let mut head_pos = (0, 0);
    let mut tail_pos = (0, 0);
    let mut pos_history: HashSet<(i32, i32)> = HashSet::new();
    pos_history.insert(tail_pos);

    for line in lines {
        let mut command = line.split(' ');
        let direction = command.next().unwrap();
        let amount = command.next().unwrap().parse::<u32>().unwrap();
        for _ in 0..amount {
            match direction {
                "R" => head_pos.0 += 1,
                "L" => head_pos.0 -= 1,
                "U" => head_pos.1 += 1,
                "D" => head_pos.1 -= 1,
                any => panic!("Uh oh unknown direction: {}", any),
            }
            if calculate_distance(head_pos, tail_pos) > 1 {
                tail_pos.1 += (head_pos.1 - tail_pos.1).signum();
                tail_pos.0 += (head_pos.0 - tail_pos.0).signum();
            }
            pos_history.insert(tail_pos);
        }
    }

    pos_history.len()
}

fn _part2(lines: &[&str]) -> usize {
    let mut knot_pos = vec![(0, 0); 10];
    let mut pos_history: HashSet<(i32, i32)> = HashSet::new();
    pos_history.insert(knot_pos[9]);
    for line in lines {
        let mut command = line.split(' ');
        let direction = command.next().unwrap();
        let amount = command.next().unwrap().parse::<u32>().unwrap();
        for _ in 0..amount {
            // new position of head
            match direction {
                "R" => knot_pos[0].0 += 1,
                "L" => knot_pos[0].0 -= 1,
                "U" => knot_pos[0].1 += 1,
                "D" => knot_pos[0].1 -= 1,
                any => panic!("Uh oh unknown direction: {}", any),
            }
            // calculate each other knot position
            for i in 1..knot_pos.len() {
                if calculate_distance(knot_pos[i - 1], knot_pos[i]) > 1 {
                    knot_pos[i].1 += (knot_pos[i - 1].1 - knot_pos[i].1).signum();
                    knot_pos[i].0 += (knot_pos[i - 1].0 - knot_pos[i].0).signum();
                }
            }
            pos_history.insert(knot_pos[9]);
        }
    }

    pos_history.len()
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_calculate_distance() {
        assert_eq!(calculate_distance((0, 0), (0, 0)), 0);
        assert_eq!(calculate_distance((1, 0), (0, 0)), 1);
        assert_eq!(calculate_distance((0, 1), (0, 0)), 1);
        assert_eq!(calculate_distance((0, 0), (1, 0)), 1);
        assert_eq!(calculate_distance((0, 0), (0, 1)), 1);
        assert_eq!(calculate_distance((0, 0), (1, 1)), 1);
        assert_eq!(calculate_distance((1, 1), (0, 0)), 1);
        assert_eq!(calculate_distance((0, 0), (-1, 1)), 1);
        assert_eq!(calculate_distance((-1, -1), (0, 0)), 1);
        assert_eq!(calculate_distance((0, 0), (2, 2)), 2);
        assert_eq!(calculate_distance((0, 0), (2, 3)), 3);
        assert_eq!(calculate_distance((-1, 3), (7, 9)), 8);
    }

    #[test]
    fn test_part1() {
        let input = "\
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), 13);
    }

    #[test]
    fn test_part2() {
        let input = "\
R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part2(&input_vec), 1);

        let input = "\
R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part2(&input_vec), 36);
    }
}

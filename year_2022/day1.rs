use std::cmp;
use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

pub fn _part1(lines: &[&str]) -> usize {
    let mut acc = 0;
    let mut max = 0;
    for line in lines {
        if line == &"" {
            max = cmp::max(max, acc);
            acc = 0;
        } else {
            acc += line.parse::<usize>().unwrap();
        }
    }
    max
}

pub fn _part2<'a>(lines: &[&str]) -> usize {
    let mut vec: Vec<usize> = vec![0];
    for line in lines {
        if line == &"" {
            // init new elf
            vec.push(0);
        } else {
            let size = vec.len();
            vec[size - 1] = vec.last().unwrap() + line.parse::<usize>().unwrap();
        }
    }

    vec.sort();
    let sum: usize = vec.iter().rev().take(3).sum();
    sum
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), 24000);
    }

    #[test]
    fn test_part2() {
        let input = "\
1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part2(&input_vec), 45000);
    }
}

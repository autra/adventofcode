use regex::Regex;
use std::collections::HashMap;
use std::fmt::Display;
use std::path::PathBuf;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn parse_fs(lines: &[&str]) -> HashMap<String, usize> {
    // some regexp
    let dir_regex = Regex::new(r"^dir (.+)$").unwrap();
    let file_regex = Regex::new(r"^(\d+) (.+)$").unwrap();
    let cd_regex = Regex::new(r"^\$ cd (.+)$").unwrap();

    let mut pwd = PathBuf::new();
    let mut directories: HashMap<String, usize> = HashMap::new();

    for line in lines {
        if line.starts_with("$ cd") {
            // cd command
            // find the current dir and set it as pwd
            let new_pwd_name = cd_regex.captures(line).unwrap().get(1).unwrap().as_str();
            if new_pwd_name == ".." {
                pwd.pop();
            } else {
                pwd.push(new_pwd_name);
            }
            // init the entry in directories
            if !directories.contains_key(pwd.to_str().unwrap()) {
                directories.insert(pwd.to_str().unwrap().to_string(), 0);
            }
        } else if dir_regex.is_match(line) {
            // nothing to do yet
        } else if file_regex.is_match(line) {
            let cap = file_regex.captures(line).unwrap();
            // in listing: files
            let old_size = directories.get(pwd.to_str().unwrap()).unwrap();
            let new_size = cap.get(1).unwrap().as_str().parse::<usize>().unwrap();
            directories.insert(pwd.to_str().unwrap().to_string(), old_size + new_size);
            // also update parents
            let mut parent = pwd.to_owned();
            loop {
                parent = match parent.parent() {
                    Some(p) => p.to_owned(),
                    None => break,
                };
                let old_size = directories.get(parent.to_str().unwrap()).unwrap();
                directories.insert(parent.to_str().unwrap().to_string(), old_size + new_size);
            }
        }
    }
    directories
}

fn _part1(lines: &[&str]) -> usize {
    let mut sum = 0;
    let directories = parse_fs(lines);
    for (_, size) in directories {
        if size <= 100000 {
            sum += size;
        }
    }
    sum
}

fn _part2(lines: &[&str]) -> usize {
    let total_space = 70000000;
    let needed_free_space = 30000000;
    let directories = parse_fs(lines);

    // calculate space taken
    let space_taken = directories.get("/").unwrap();

    let free_space = total_space - space_taken;
    let space_to_free = needed_free_space - free_space;

    directories
        .into_values()
        .filter(|&val| val >= space_to_free)
        .min()
        .unwrap()
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), 95437);
    }

    #[test]
    fn test_part2() {
        let input = "\
$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part2(&input_vec), 24933642);
    }
}

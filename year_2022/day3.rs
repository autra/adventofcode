use std::fmt::Display;

pub fn part1(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part1(lines))
}
pub fn part2(lines: &[&str]) -> Box<dyn Display> {
    Box::new(_part2(lines))
}

fn _part1(lines: &[&str]) -> usize {
    let mut answer = 0;
    for line in lines {
        let compartment1 = &line[..line.len() / 2];
        let compartment2 = &line[line.len() / 2..];
        let common_char = compartment1
            .chars()
            .find(|c| compartment2.contains(*c))
            .unwrap();
        let common_char_num = common_char as usize;
        let priority;
        if common_char_num <= 90 {
            priority = common_char_num - 64 + 26; // A is 65
        } else {
            priority = common_char_num - 96;
        }
        answer += priority;
    }
    answer
}

fn _part2(lines: &[&str]) -> usize {
    let mut answer = 0;
    for i in (0..lines.len()).step_by(3) {
        let e1 = lines[i];
        let e2 = lines[i + 1];
        let e3 = lines[i + 2];

        let common_chars_12 = e1.chars().filter(|c| e2.contains(*c));
        let common_chars_123: Vec<char> = common_chars_12.filter(|c| e3.contains(*c)).collect();
        // at this point we have only one common char BUT it can be duplicated in rucksa
        let common_char_num = common_chars_123[0] as usize;
        let priority;
        if common_char_num <= 90 {
            priority = common_char_num - 64 + 26; // A is 65
        } else {
            priority = common_char_num - 96;
        }
        answer += priority;
    }
    answer
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_part1() {
        let input = "\
vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part1(&input_vec), 157);
    }

    #[test]
    fn test_part2() {
        let input = "\
vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";
        let input_vec: Vec<&str> = input.split('\n').collect();
        assert_eq!(_part2(&input_vec), 70);
    }
}

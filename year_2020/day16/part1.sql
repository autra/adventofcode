with possible_interval as (
  select
    split_part(u, '-', 1)::int as min,
    split_part(u, '-', 2)::int as max

  from
    day16,
    regexp_matches(line, '\w*: (\d*-\d*) or (\d*-\d*)') matches,
    lateral unnest(matches) u
  where
    line_number < (select line_number - 1 from day16 where line = 'your ticket:')

),
nearby_tickets as (
  select
    line_number as ticket_number, val::int
  from
    day16,
    lateral unnest(regexp_split_to_array(line, ',')) val
  where
    line_number > (select line_number from day16 where line = 'nearby tickets:')
)
select
  sum(val)
from
  nearby_tickets
where
  not exists(select 1 from possible_interval where val between min and max)
;

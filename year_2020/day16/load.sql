drop table if exists day16;

create table day16(line_number integer not null generated always as identity, line text);

\copy day16(line) from './input'

vacuum analyse day16;

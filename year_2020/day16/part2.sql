with recursive possible_interval as (
  -- parse the first part of input : category and interval
  select
    matches[1] as category,
    -- postgresql multirange FTW
    int4multirange(
      int4range(matches[2]::int, matches[3]::int, '[]'),
      int4range(matches[4]::int, matches[5]::int, '[]')
    ) domain
  from
    day16,
    regexp_matches(line, '^(.*): (\d*)-(\d*) or (\d*)-(\d*)') matches
  where
    line_number < (select line_number - 1 from day16 where line = 'your ticket:')
),
-- explode tickets by line number and columns
exploded_tickets as (
  select
    line_number as ticket_number, val::int, col
  from
    day16,
    lateral unnest(regexp_split_to_array(line, ',')) with ordinality as ticket(val, col)
  where
    line_number > (select line_number from day16 where line = 'nearby tickets:')
),
-- keep only what's valid
valid_tickets as (
  select
    *
  from
    exploded_tickets t
  where
    -- ticket are valids when there is no invalid values
    not exists (
      select
        1
      from
        exploded_tickets invalid
      where
        t.ticket_number = invalid.ticket_number
        -- invalid values: when there is no possible interval that matches
        and not exists (select 1 from possible_interval where domain @> val)
    )
),
-- get all the possible positions for each category
possible_positions as (
  select
    row_number() over(order by cardinality(cols)) as iid, *
  from (
    select
      i.*, array_agg(col) cols
    from
      possible_interval i,
      lateral (
          -- a position is possible when every value of this position is in the
          -- interval of the given category
          select
             col
          from
            valid_tickets
          group by col
          having bool_and(domain @> val)
      ) col
    group by i.category, i.domain
  ) _
),
-- next 2 CTE just do the calculation to link every category to the column name
-- some category match every column so we just start with the one that matches
-- the less and walk through every possibily
tree_walker as (
  select 1 as current_interval_id, '{}'::jsonb as map, ARRAY[]::bigint[] as taken_pos
  UNION
  select
    current_interval_id + 1 as current_interval_id,
    map || jsonb_build_object(pp.category, colid) as map,
    taken_pos || ARRAY[colid] as taken_pos
  from
    tree_walker
    join possible_positions pp on current_interval_id=pp.iid,
    unnest(pp.cols) colid
  where
    colid <> all (taken_pos)
),
column_by_interval as (
  select
    map
  from
    tree_walker
  order by current_interval_id desc
  limit 1
),
-- parse my ticket
my_ticket as (
  select
    regexp_split_to_array(line, ',') vals
  from
    day16
  where
    line_number=(select line_number from day16 where line='your ticket:') + 1
),
-- select all the value of my ticket corresponding to departure* category
nearly_there as (
  select
    row_number() over(), vals[v::int]::int as val
  from
    my_ticket,
    column_by_interval,
    jsonb_each(map) elem(k, v)
  where
    k like 'departure%'
),
-- postgresql does not have any mult aggregation fn :-(
mult as (
  select 1 as pt, 1::bigint as acc
  union
  select pt+1 as pt, acc * val::bigint
  from
    mult
    join nearly_there on pt=row_number
)
select acc as "Answer!!" from mult order by pt desc limit 1
;

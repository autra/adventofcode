with recursive iterations as (
  select
    count(*) as it, jsonb_object_agg(v, array[t.o]) as input, last_value
  from
    day15,
    lateral (
      select
        v, o, last_value(v) over (order by o rows between unbounded preceding and unbounded following)
      from unnest(string_to_array(line, ',')) with ordinality  as input(v, o)
    ) t
  group by last_value
  union all
  select
    it+1 as it,
    case
      when input->new_val.v is null then
        input || jsonb_build_object(new_val.v, ARRAY[it+1])
      else
        input
        || jsonb_build_object(
            new_val.v,
            input->new_val.v
              || jsonb_build_array((input->new_val.v)[jsonb_array_length(input->new_val.v) -1], it+1)
          )
    end,
    new_val.v
  from
    iterations,
    lateral (
      select
        cast(case
          when jsonb_array_length(input->last_value) = 1 then
            0
          else
            -- note jsonb arrays are indexed from 0 :-/
            (input->last_value)[jsonb_array_length(input->last_value) - 1]::integer -
            (input->last_value)[jsonb_array_length(input->last_value) - 2]::integer
        end as text)
    ) new_val(v)
  where
    it < 2020
)
select
last_value
--  input[jsonb_array_length(input)] as "Answer!!"
from
  iterations
order by it desc
limit 1;

-- it is not doable even in plpgsql until I find a way not to copy input at each iteration
-- ok, part1 does not scale and probably won't, even if we implement the loop in plpgsql. It's because we copy the jsonb that serves as index each iteration, and it's more and more expensive to do so.
-- So I relax my own rules (one query in sql) with 2 things:
-- - I'm using a table as a "cache/index", to answer quickly to the question "when was this number last spoken"
-- - so I need plpgsql to have several snapshots (I cannot do it in sql, even with "insert" in cte
--
-- On my machine this still takes 18min.

create table if not exists turns_by_numbers (
  num integer not null,
  turn integer not null
);
truncate table turns_by_numbers;
create index if not exists number_idx on turns_by_numbers(num, turn);
insert into turns_by_numbers
  select
    num::int,
    idx
  from
    day15,
    unnest(string_to_array(line, ',')) with ordinality as _(num, idx);


do $$
declare
max_turn integer = 30000000;
interval integer = max_turn / 100;
it int;
prev_it int;
last_v int;
begin

  select num, turn into last_v, it from turns_by_numbers where turn=(select max(turn) from turns_by_numbers);

  for i in it..(max_turn-1) loop
    prev_it = (select max(turn) from turns_by_numbers where turn != i and num=last_v);
    if prev_it is null then
      last_v = 0;
    else
      last_v = i - prev_it;
    end if;

    insert into turns_by_numbers(num, turn) values(last_v, i+1);
    if i % interval = 0 then
      raise notice '% %%', round(i::decimal * 100 / max_turn);
    end if;
  end loop;

  raise notice 'Answer: %', last_v;
end;
$$
;

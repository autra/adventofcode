drop table if exists day15;

create table day15(line_number integer not null generated always as identity, line text);

\copy day15(line) from './input'

vacuum analyse day15;

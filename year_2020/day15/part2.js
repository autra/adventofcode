const input = [1,0,18,10,19,6];
//const length = 2020;
const length = 30000000;
const cache = new Map();

let i = 1;
let spoken_number = null;
for (val of input) {
  spoken_number = val;
  if (i < input.length) {
    cache.set(spoken_number, i);
  }
  i++;
}

for (let idx = i; idx <= length ; idx++) {
  let prev_id = cache.get(spoken_number);
  cache.set(spoken_number, idx - 1);

  spoken_number = prev_id ? idx - 1 - prev_id : 0;
}

console.log('Answer: ', spoken_number);

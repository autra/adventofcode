drop table if exists day9;

create table day9(row_number serial, num bigint);

\copy day9(num) from './input'

vacuum analyse day9;

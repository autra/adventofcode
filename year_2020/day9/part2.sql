-- this works but it is very slow imho (~100-150 ms)
with w_length(l) as (
  select s from generate_series(2, (select count(*) from day9)) s
)
select min+max as answer
from w_length, lateral (
  select min(num) over w, max(num) over w, sum(num) over w as sum
  from day9
  window w as (order by row_number rows between w_length.l preceding and current row)
) w_sum
where max < 88311122
    and w_sum.sum=88311122 -- answer to part1

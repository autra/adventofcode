-- this works but it is very slow imho (~100-150 ms)
select d.row_number, d.num from day9 d
left join lateral (
  select a.num+b.num as sum
  from day9 a, day9 b
  where a.row_number between d.row_number-25 and d.row_number - 1
    and b.row_number between d.row_number - 25 and d.row_number - 1
    and a.num+b.num=d.num
) s on true
where s.sum is null and d.row_number > 24
order by d.row_number asc
limit 1;

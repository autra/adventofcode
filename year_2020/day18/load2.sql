drop table if exists day18;

create table day18(line_number integer not null generated always as identity, line text);

\copy day18(line) from './input2'

vacuum analyse day18;

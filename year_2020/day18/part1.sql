-- keeping this for archeological purpose, because the algo I used in part2 is
-- much much better, and can be used here too
select
  sum(result) as "Answer!!"
from
  day18,
  lateral (
    -- iterate on each character
    with recursive vm as (
      select
        1 as pt,
       -- '' as char,
        ARRAY[0::bigint] as acc,
        ARRAY[]::text[] as ops

      union all

      select
        pt+1 as pt,
        --current_char as char,
        case
          when current_char = '(' then
            array_append(acc, 0)
            --array_append(acc, 0)
          when current_char = ')' then
            -- jump back up one level: if there is ops, do the ops and acc
            -- if not, we are the first, just jump one level up
            case
              when ops[cardinality(ops) - 1] is null then
                -- TODO postgresql supports [start:end] notation!!
                array_append(trim_array(acc, 2), acc[cardinality(acc)])
              else
                case
                  when ops[cardinality(ops) - 1] = '+' then
                    array_append(
                      trim_array(acc, 2),
                      acc[cardinality(acc) - 1] + acc[cardinality(acc)]
                    )
                  else
                    array_append(
                      trim_array(acc, 2),
                      acc[cardinality(acc) - 1] * acc[cardinality(acc)]
                    )
                end
            end
          when current_char != all(array[' ', '+', '*']) then
            -- its a number
            case
              -- if we don't have ops, it's the firt number of a block -> our new acc
              when ops[cardinality(ops)] is null then
                array_append(trim_array(acc, 1), current_char::int)
              else
                -- if we do have an ops, do the math and use this value as acc
                array_append(
                  trim_array(acc, 1),
                  case
                    when ops[cardinality(ops)] = '+' then
                      acc[cardinality(acc)] + current_char::int
                    else
                      acc[cardinality(acc)] * current_char::int
                  end)
            end
          else
            acc
            -- do nothing
        end as acc,
        case
          when current_char = '(' then
            array_append(ops, null)
          when current_char = ')' then
            trim_array(ops, 1)
          when current_char = any(array['+', '*']) then
            array_append(trim_array(ops, 1), current_char)
          else
            ops
        end as ops
      from
        vm,
        lateral substring(line from pt for 1) current_char
      where
        current_char is not null and current_char != ''
    )
    select acc[1] as result from vm
    order by pt desc limit 1
  ) as vm
--  where
  --  line_number = 2
;

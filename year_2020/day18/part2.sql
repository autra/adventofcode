with recursive
exec as (
  select
    1 as it,
    line as orig_line,
    line as line,
    null::text[] as ma_plus,
    null::text[] as ma_mult,
    null::text[] as ma_paren
  from
    day18

  union all

  select
    it+1 as it,
    orig_line,
    case
    when m_paren is not null then
      regexp_replace(line, '\(\d+\)', m_paren[1])
    when m_plus is not null then
      regexp_replace(line, '\d+ \+ \d+', (m_plus[1]::bigint + m_plus[2]::bigint)::text)
    when m_mult is not null then
      regexp_replace(line, '\d+ \* \d+[^(]*$', (m_mult[1]::bigint * m_mult[2]::bigint)::text || m_mult[3])
    else
      line
    end,
    m_plus,
    m_mult,
    m_paren
  from
    exec,
    regexp_match(line, '\((\d+)\)') m_paren,
    regexp_match(line, '(\d+) \+ (\d+)') m_plus,
    -- we absolutely want to force parenthesis removal *before* calculating
    -- multiplication, so we only calculate them if there is no parenthesis
    -- after
    -- otherwise situation like: 1 * 2 + 1 * 2 first (2+3) will evaluate 1 * 2 first
    regexp_match(line, '(\d+) \* (\d+)([^(]*)$') m_mult
  where line ~ '[+*()]'
)
select sum(line::bigint) as "Answer!!" from exec
where line !~ '[+*()]'
;

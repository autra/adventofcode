with
grouped as (
  select
    line_number,
    line,
    count(*)
      filter (where line ~ 'Tile')
      over (rows between unbounded preceding and current row) group_id
  from "2020".day20
  where line != ''
),
grouped_with_y as (
  select
    line,
    first_value((regexp_match(line, 'Tile (.*):'))[1]) over (partition by group_id) as tile_id,
    rank() over (partition by group_id order by line_number)-1 as y
  from grouped
), exploded as (
  select
    tile_id,
    c,
    x,
    y
  from grouped_with_y,
    lateral regexp_split_to_table(line, '') with ordinality as s(c, x)
  where y != 0
),
sides as (
  select
    tile_id,
    string_agg(c, '' order by y) filter(where x = 1) as chars,
    'left' as side
  from
    exploded
  group by tile_id
  union all
  select
    tile_id,
    string_agg(c, '' order by y) filter(where x = 10) as chars,
    'right' as side
  from
    exploded
  group by tile_id
  union all
  select
    tile_id,
    string_agg(c, '' order by x) filter(where y = 1) as chars,
    'top' as side
  from
    exploded
  group by tile_id
  union all
  select
    tile_id,
    string_agg(c, '' order by x) filter(where y = 10) as chars,
    'bottom' as side
  from
    exploded
  group by tile_id

),
corners as (
  select s1.tile_id, count(*) as count
  from sides s1
    join sides s2 on s1.tile_id != s2.tile_id and (s1.chars = s2.chars or s1.chars = reverse(s2.chars))
  group by s1.tile_id
  having count(*) = 2
)
select
  res[1]::bigint * res[2]::bigint * res[3]::bigint * res[4]::bigint as "Answer!!"
from (
  select array_agg(tile_id) res
  from corners
) _
;

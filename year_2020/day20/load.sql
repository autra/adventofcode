drop table if exists "2020".day20;

create table "2020".day20(line_number integer not null generated always as identity, line text);

\copy "2020".day20(line) from './input'

vacuum analyse "2020".day20;

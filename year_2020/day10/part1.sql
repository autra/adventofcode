with diff(v) as (
  select jolt - coalesce(lag(jolt, 1) over (order by jolt), 0) as v from day10
)
select count(v) filter (where v=1) * (count(v) filter (where v=3)+1) as "Answer part1!!"
from diff;

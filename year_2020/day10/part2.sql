-- get an ordered list of jolts
with recursive with_outlet as (
  -- add back the outlet
  select jolt from day10
  UNION
  values(0)
),
ordered as (
  select row_number() over (order by jolt) as line_number, jolt from with_outlet
),
-- get the last one (more intuitive for me this way, although I'm pretty sure it's symmetrical)
max as (
  select line_number, jolt from ordered order by jolt desc limit 1
),
-- algorithm:
-- - start with last jolt : there is 1 possible path
-- - go back one line : there is still one possibility
-- - go back one more line: there is 1 possibility (the line above) + one other if the last jolt is near enough
-- - go back one more line: there are the number of possible seq of the line
--   just above + the number of possible seq of the line + 2 if reachable, and same
--   thing for line +3
-- it's like we are plugging subtrees into a root.
--
-- implementation:
-- - keep the last 3 jolt values and their aggregate (number of possible path reachable from them)
-- - also keep the line_number values. In rigor I only need the last one to join on "ordered", but it makes debugging easier
start(line_numbers, jolts, agg) as (
  select
    ARRAY[line_number, null, null] as line_numbers,
    ARRAY[jolt, null, null] as jolts,
    ARRAY[1::bigint, null, null] as agg
  from max

  UNION

  select
    ARRAY[d.line_number, s.line_numbers[1], s.line_numbers[2]],
    ARRAY[d.jolt, s.jolts[1], s.jolts[2]],
    ARRAY[
      s.agg[1]
        + case when s.jolts[2] - d.jolt <=3 then s.agg[2] else 0 end
        + case when s.jolts[3] - d.jolt <= 3 then s.agg[3] else 0 end,
      s.agg[1],
      s.agg[2]
    ]
  from ordered d
  -- go under 1 line
  join start s on d.line_number=s.line_numbers[1]-1
  where d.line_number > 0 -- stop condition, should be useless
)
select agg[1] as "Answer part 2"
from start
where line_numbers[1] = 1;

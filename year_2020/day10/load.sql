drop table if exists day10;

create table day10(line_number integer not null generated always as identity, jolt bigint);

\copy day10(jolt) from './input'

vacuum analyse day10;

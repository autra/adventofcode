drop table if exists day19;

create table day19(line_number integer not null generated always as identity, line text);

\copy day19(line) from './input2'

vacuum analyse day19;

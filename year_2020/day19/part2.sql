with recursive
rules_int as (
  select (regexp_match(line, '(\d+):'))[1] as current, regexp_split_to_array(trim(next, '"'), ' ') next
  from day19,
    lateral regexp_matches(line, '\d+: (.*)') rules,
    lateral regexp_split_to_table(rules[1], ' \| ') as next
    where
      line ~ '\d+:'
      and next is not null
),
rules as (
  select
    current,
    next
  from
    rules_int
  where
    current not in ('8', '11')

  union all

  values
    ('8', ARRAY['42']),
    ('8', ARRAY['42', '8']),
    ('11', ARRAY['42', '31']),
    ('11', ARRAY['42', '11', '31'])
),
messages as (select regexp_split_to_array(line, '') as message from day19 where line ~ '^[ab]+$'),
it as (
  select message, next as rules, array[row(message, next)] as path, false as is_cycle
  from rules, messages
  where current = '0'

  union all

  (
    with previous_it as ( table it)

    select
      message,
      r2.next || previous_it.rules[2:],
      previous_it.path || row(message, r2.next),
      row(message, r2.next) = any (previous_it.path)
    from
      previous_it
      join rules r2 on previous_it.rules[1] = r2.current
    where
      not previous_it.is_cycle

    union all

    select
      message[2:],
      rules[2:],
      path,
      is_cycle
    from
      previous_it
    where
      message[1] = rules[1]
      and rules[1] in ('a', 'b')
      and not previous_it.is_cycle
  )
)
--  select * from it limit 100000;

select count(*) from it where rules = '{}' and message='{}';

with recursive
split as (
  select
    line_number
  from day19
  where line = ''
),
parsed as (
  select
    split_part(line, ': ', 1) as id,
    split_part(line, ': ', 2) as inst
  from
    day19,
    split
  where day19.line_number < split.line_number
),
input as (
  select
    line
  from
    day19,
    split
  where day19.line_number > split.line_number
),
regex as (
  select 0 as count, inst as reg
  from parsed
  where id='0'

  union all

  select
    count + 1 as count, regexp_replace(reg, '\d+', subst) as reg
  from
    regex,
    regexp_match(reg, '(\d+)') match,
    lateral (
      select
        case
        when inst ~ '"[a-z]"' then
          regexp_replace(inst, '"([a-z])"', '\1')
        else
          '(' || inst || ')'
        end as subst
      from
        parsed
      where id=match[1]
    ) _
  where reg ~ '\d'
  --and count < 10
)
--  select * from regex
select
  count(*) filter (where regexp_match(line, '^' || reg || '$', 'x') is not null) as "Answer!!"
from
  input,
  regex
where reg !~ '\d'

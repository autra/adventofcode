drop table if exists day17;

create table day17(line_number integer not null generated always as identity, line text);

\copy day17(line) from './input'

vacuum analyse day17;

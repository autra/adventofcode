with recursive
parsed as (
  select
    x, y, 0 as z, 0 as w, (l='#')::int as activated
  from
    day17 as d(y, line),
    unnest(regexp_split_to_array(line, '')) with ordinality as _(l, x)
),
cycle(cycle, x, y, z, w, activated) as (
  select
    0 as cycle, x, y, 0 as z, 0 as w, activated
  from parsed

  union all
  (
    with previous as (
      select * from cycle
    ),
    next as (
      select
        p.cycle+1 as cycle,
        nx,
        ny,
        nz,
        nw,
        (case when activated::bool
            then sum_neighbours between 2 and 3
            else sum_neighbours = 3
          end)::int as activated
      from
        (select cycle from previous limit 1) p,
        generate_series((select min(x) from previous) - 1, (select max(x) from previous) + 1) nx,
        generate_series((select min(y) from previous) - 1, (select max(y) from previous) + 1) ny,
        generate_series((select min(z) from previous) - 1, (select max(z) from previous) + 1) nz,
        generate_series((select min(w) from previous) - 1, (select max(w) from previous) + 1) nw
        left join lateral (
          select sum(activated) as sum_neighbours
          from previous cycle
          where
            cycle.x between nx - 1 and nx + 1
            and cycle.y between ny - 1 and ny + 1
            and cycle.z between nz - 1 and nz + 1
            and cycle.w between nw - 1 and nw + 1
            and not (cycle.x = nx and cycle.y = ny and cycle.z = nz and cycle.w = nw)
        ) _1 on true
        left join lateral ( select activated from previous where nx=x and ny=y and nz=z and nw=w) _2 on true
      where
        cycle < 6
    )
    -- filter side that are empty
    select
      cycle, nx, ny, nz, nw, activated
    from next
    where activated::bool

  )
)
select
  count(*) filter (where activated::bool)
from cycle
where cycle=(select max(cycle) from cycle);

-- uncomment me to debug ;-)
--  select
  --  cycle, z, w, string_agg(agg, E'\n' order by y) agg
--  from (
  --  select cycle, y, z, w, string_agg(activated::text, ' ' order by x) agg from cycle group by cycle, y, z, w
--  ) y_agg
--  group by cycle, z, w

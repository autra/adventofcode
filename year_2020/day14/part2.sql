with recursive
line as (
  select
    line_number,
    m[1] as op,
    m[2] as mempt,
    m[3] as val
  from
    day14,
    lateral regexp_matches(line, '(\w+)(?:\[(\d+)\])? = (\w+)') m
),
vm as (
  select 1 as pt, '{}'::jsonb as mem, repeat('X', 36) as mask

  union

  select
    pt+1,
    case
      when op = 'mem' then
        -- TODO
        vm.mem || new_mem.mem_part
      else
        vm.mem
    end as mem,
    case
      when op = 'mask' then
        line.val
      else
        vm.mask
    end as mask
  from
    vm
    join
      line on line.line_number = vm.pt,
      lateral(
        with recursive addr_mask as (
          select string_agg(bits, '' order by o) as mask
          from (
            select
              o,
              case
                when mask = '0' then
                  mempt
                else
                  mask
              end as bits
            from rows from (unnest(
                string_to_array(vm.mask, null),
                string_to_array(line.mempt::bigint::bit(36)::text, null)
            )) with ordinality as _(mask, mempt, o)
          ) _
        ),
        mem_addrs as (
          select 1 as it, addr_mask.mask
          from addr_mask
          union
          select it+1 as it, regexp_replace(mask, '([01]*)X(.*)', '\1' || c || '\2') mask
          from
            mem_addrs,
            generate_series(0, 1) c
          where
            mask like '%X%'
        )
        select
          jsonb_object_agg(mask, line.val) mem_part
        from
          mem_addrs
        where
          mask not like '%X%'
          and line.op = 'mem'
      ) new_mem
)
--  select * from vm
,
final_state as (
  select mem
  from vm
  order by pt desc
  limit 1
)
select sum(value::bigint) as "Answer!!"
from final_state, jsonb_each_text(mem) ;

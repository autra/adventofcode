drop table if exists day14;

create table day14(line_number integer not null generated always as identity, line text);

\copy day14(line) from './input'

vacuum analyse day14;

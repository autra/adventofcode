with recursive
line as (
  select
    line_number,
    m[1] as op,
    m[2] as mempt,
    m[3] as val
  from
    day14,
    lateral regexp_matches(line, '(\w+)(?:\[(\d+)\])? = (\w+)') m
),
vm as (
  select 1 as pt, '{}'::jsonb as mem, repeat('X', 36) as mask

  union

  select
    pt+1,
    case
      when op = 'mem' then
        vm.mem || jsonb_build_object(line.mempt, new_mem.sum)
      else
        vm.mem
    end as mem,
    case
      when op = 'mask' then
        line.val
      else
        vm.mask
    end as mask
  from
    vm
    join
      line on line.line_number = vm.pt,
      lateral (
        -- this does the bitmap arithmetic
        select
          -- aggregate everything
          sum((bit::bit(36) >> bitpt - 1)::bigint)::bigint::bit(36)
        from (
          -- calculate each bit of index bitpt
          select
            bitpt,
            case
              when
                substring(vm.mask from bitpt for 1) = 'X' then
                  get_bit(line.val::int::bit(36), bitpt - 1)::bit
              else
                substring(vm.mask from bitpt for 1)::bit
            end as bit
          from
            generate_series(1, 36) bitpt
        ) bitmap
        where
          line.op = 'mem'
      ) new_mem
),
final_state as (
  select mem
  from vm
  order by pt desc
  limit 1
)
select sum(value::bit(36)::bigint) as "Answer!!"
from final_state, jsonb_each_text(mem) ;

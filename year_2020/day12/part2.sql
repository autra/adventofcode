with recursive parsed as (
  select line_number as id, left(inst, 1) as inst, (regexp_match(inst, '^[A-Z](\d*)'))[1]::int as arg
  from day12
),
vm as (
  select 1 as ip, 0 as ship_north, 0 as ship_east, 1 as wp_north, 10 as wp_east

  UNION

  select
    ip+1,
    -- ship position
    ship_north + case when inst='F' then wp_north * arg else 0 end,
    ship_east + case when inst='F' then wp_east * arg else 0 end,

    case
    when inst='N' then wp_north + parsed.arg
    when inst='S' then wp_north - parsed.arg
    when inst='L' and arg=90 or inst='R' and arg=270 then wp_east
    when (inst='L' or inst='R') and arg=180 then -wp_north
    when inst='L' and arg=270 or inst='R' and arg=90 then -wp_east
    else wp_north
    end as wp_north,

    case
    when inst='E' then wp_east + parsed.arg
    when inst='W' then wp_east + -parsed.arg
    when inst='L' and arg=90 or inst='R' and arg=270 then -wp_north
    when (inst='L' or inst='R') and arg=180 then -wp_east
    when inst='L' and arg=270 or inst='R' and arg=90 then wp_north
    else wp_east
    end as wp_east

  from vm join parsed on vm.ip=parsed.id
)
select abs(ship_east) + abs(ship_north) as "Answer!!" --, ip, inst, arg, ship_east, ship_north, wp_east, wp_north --
from vm
-- left join parsed on vm.ip-1=parsed.id
order by ip desc
limit 1
;

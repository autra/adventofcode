with recursive parsed as (
  select line_number as id, left(inst, 1) as inst, (regexp_match(inst, '^[A-Z](\d*)'))[1]::int as arg
  from day12
),
vm as (
  select 1 as ip, 0 as north, 0 as east, 0 as east_angle

  UNION

  select
    ip+1,
    north + case
      when inst='N' or inst='F' and east_angle=90 then parsed.arg
      when inst='S' or inst='F' and east_angle=270 then -parsed.arg
      else 0
    end as north,
    east + case
      when inst='E' or inst='F' and east_angle=0 then parsed.arg
      when inst='W' or inst='F' and east_angle=180 then -parsed.arg
      else 0
    end as east,
    (east_angle + 360 + case
      when inst='L' then parsed.arg
      when inst='R' then -parsed.arg
      else 0
    end) % 360 as east_angle
  from vm join parsed on vm.ip=parsed.id
)
select abs(east) + abs(north) as "Answer!!"
from vm
order by ip desc
limit 1
;

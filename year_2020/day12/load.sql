drop table if exists day12;

create table day12(line_number integer not null generated always as identity, inst text);

\copy day12(inst) from './input'

vacuum analyse day12;

drop table if exists day11;

create table day11(line_number integer not null generated always as identity, seat_rows text);

\copy day11(seat_rows) from './input'

vacuum analyse day11;

-- many thanks to pg_xocolatl for the inspiration
with recursive
maxy as (
  select count(*) as v from day11
),
maxx as (
  select length(seat_rows) as v from day11 limit 1
),
occupied(rev, seats) as (
  select 1 as rev, string_agg(seat_rows, '' order by line_number)
  from day11

  UNION
  select o.rev+1, news.v
  from occupied o, lateral (
    select string_agg(new.v, '') as v
    from maxx, maxy,
    lateral (
      select
        case
        when seat='#' and num_occupied >= 4 then 'L'
        when seat='L' and num_occupied = 0 then '#'
        else seat
        end
      from (
        select
        s.seat,
        coalesce((not side.l and (lag(s.seat, 1) over (order by idx))='#')::int, 0)
        + coalesce((not side.r and (lead(s.seat, 1) over (order by idx))='#')::int, 0)

        + coalesce((not side.l and (lag(s.seat, maxx.v+1) over (order by idx))='#')::int, 0)

        + coalesce(((lag(s.seat, maxx.v) over (order by idx))='#')::int, 0)

        + coalesce((not side.r and (lag(s.seat, maxx.v-1) over (order by idx))='#')::int, 0)

        + coalesce((not side.l and (lead(s.seat, maxx.v-1) over (order by idx))='#')::int, 0)

        + coalesce(((lead(s.seat, maxx.v) over (order by idx))='#')::int, 0)

        + coalesce((not side.r and (lead(s.seat, maxx.v+1) over (order by idx))='#')::int, 0)

        from unnest(string_to_array(o.seats, null)) with ordinality as s(seat, idx),
             lateral (values((idx-1) % maxx.v = 0, (idx-1) % maxx.v = (maxx.v - 1))) as side(l, r)
        ) as s(seat, num_occupied)

    ) as new(v)
  ) as news
  where news.v <> o.seats
)
select length(translate(o.seats, 'L.', '')) from occupied o
order by rev desc
limit 1
;

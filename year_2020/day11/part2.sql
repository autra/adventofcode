with recursive
-- we'll need the column size
maxx as (
  select length(seat_rows) as v from day11 limit 1
),
-- we concatenate each iteration input on one line and use unnest to treat each elemnt one by one
-- we can get the column number quite easily with (idx-1) % maxx.v anyway
occupied(rev, seats) as (
  select 1 as rev, string_agg(seat_rows, '' order by line_number)
  from day11

  UNION

  select o.rev+1, news.v
  from occupied o, lateral (
    select string_agg(new.v, '' order by idx) as v
    from maxx,
    lateral (
      -- calculate new seat value
      select
        idx,
        case
        when seat='#' and num_occupied >= 5 then 'L'
        when seat='L' and num_occupied = 0 then '#'
        else seat
        end
      from (
        select
        s.idx,
        s.seat,
        ----------------------
        -- let's count the # --
        -----------------------
        -- Principle :
        -- - use window function everywhere with carefully crafted partitions to select the right char set
        -- - aggregate this char set
        -- - use translate to remove . from the set
        -- - inspect the correct side of the string for L or # with `left` or `right`

        -- same row, before char
        cast(
          right(
            translate(
              coalesce(
                string_agg(seat, '') over(partition by line_number.v
                                          order by idx rows between unbounded preceding and current row
                                          exclude current row),
                ''),
              '.', ''),
            1) = '#'
          as int
        )
        -- same row, after
        +
        cast(
          left(
            translate(
              coalesce(
                string_agg(seat, '') over(partition by line_number.v
                                          order by idx rows between current row and unbounded following
                                          exclude current row),
                ''),
              '.', ''),
            1) = '#'
          as int
        )
        -- same column, before
        +
        cast(
          right(
            translate(
              coalesce(
                string_agg(seat, '') over(partition by (col_number.v)
                                          order by line_number.v rows between unbounded preceding and current row
                                          exclude current row),
                ''),
              '.', ''),
            1) = '#'
          as int
        )
        -- same column, after
         +
        cast(
          left(
            translate(
              coalesce(
                string_agg(seat, '') over(partition by (col_number.v)
                                          order by line_number.v rows between current row and unbounded following
                                          exclude current row),
                ''),
              '.', ''),
            1) = '#'
          as int
        )
        -- diagonal from top-left to bottom-right, before current
        +
        cast(
          right(
            translate(
              coalesce(
                string_agg(seat, '') over(partition by (line_number.v-col_number.v)
                                          order by line_number.v rows between unbounded preceding and current row
                                          exclude current row),
                ''),
              '.', ''),
            1) = '#'
          as int
        )
        -- diagonal from top-left to bottom-right, after current
        +
        cast(
          left(
            translate(
              coalesce(
                string_agg(seat, '') over(partition by (line_number.v-col_number.v)
                                          order by line_number.v rows between current row and unbounded following
                                          exclude current row),
                ''),
              '.', ''),
            1) = '#'
          as int
        )
        -- diagonal from bottom-left to top-right, before current
        +
        cast(
          right(
            translate(
              coalesce(
                string_agg(seat, '') over(partition by (line_number.v+col_number.v)
                                          order by line_number.v rows between unbounded preceding and current row
                                          exclude current row),
                ''),
              '.', ''),
            1) = '#'
          as int
        )
        -- diagonal from bottow-left to top-right, after current
        +
        cast(
          left(
            translate(
              coalesce(
                string_agg(seat, '') over(partition by (line_number.v+col_number.v)
                                          order by line_number.v rows between current row and unbounded following
                                          exclude current row),
                ''),
              '.', ''),
            1) = '#'
          as int
        )

        -- expand the string to a table containing chars and index
        from unnest(string_to_array(o.seats, null)) with ordinality as s(seat, idx),
        --  the line_number is the quotient of the integer div between this index and line length
        lateral (values( (idx - 1) / maxx.v) ) as line_number(v),
        -- the col number is the rest (modulo)
        lateral (values( (idx - 1) % maxx.v) ) as col_number(v)
      ) as s(idx, seat, num_occupied)
    ) as new(idx, v)
  ) as news
  -- stop condition when next line is same as previous : we achieve stability
  where news.v <> o.seats
)
select length(translate(o.seats, 'L.', '')) as "Answer!!"
from occupied o, lateral generate_series(1, 100, 10) n
order by rev desc, n
limit 1
;

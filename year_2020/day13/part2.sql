with recursive
bus as (
  select u.idx-1 as idx, u.v::int as interval
  from day13,
  unnest(string_to_array(line, ',')) with ordinality as u(v, idx)
  where line_number=2
  and u.v!='x'
),
-- so t = q.int - idx
-- in other word, t = -idx mod(interval)
-- which doesn't really make sense, so let's add interval until the form is normalized
good_modulo as (
  select
    row_number() over (order by r desc) as id,
    -- normalize n
    cast(n as bigint),
    case when r < 0 then r+n else r end as r
  from (
    select
      idx,
      interval as n,
      (interval-idx) % interval as r
    from bus
    order by r desc
  ) gm
),
max as (
  select max(id) v from good_modulo
),
chinese_remainder_theorem(depth, r1, n1, r2, n2) as (
  select
    1,
    -- cast all the things to numeric 20 to have enough digits!
    g1.r::numeric(20,0) as r1,
    g1.n::numeric(20,0) as n1,
    g2.r::numeric(20,0) as r2,
    g2.n::numeric(20,0) as n2
  from
    good_modulo g1
  join
    good_modulo g2 on g2.id=2 and g1.id=1
  union
  select
    c.depth+1,
    normalized_x.v::numeric(20, 0) as r1,
    (n1*n2)::numeric(20, 0) as n1,
    g.r::numeric(20, 0) as r2,
    g.n::numeric(20, 0) as n2
  from
    max m,
    chinese_remainder_theorem c
  left join
    good_modulo g on g.id=c.depth+2,
  lateral(
    with recursive extended_euclidean(depth, n1, s1, t1, n2, s2, t2) as (
      select 1 as depth,
             c.n1,
             1::numeric(20, 0) as s1,
            0::numeric(20, 0) as t1, c.n2, 0::numeric(20, 0) as s2, 1::numeric(20, 0) as t2
      union
      select
        depth+1 as depth,
        n2,
        s2,
        t2,
        (n1 % n2)::numeric(20, 0),
        (s1 - s2 * floor(n1 / n2))::numeric(20, 0),
        (t1 - t2 * floor(n1 / n2))::numeric(20, 0)
      from extended_euclidean
      where n2 <> 0
    )
    select s1 as m1, t1 as m2
    from extended_euclidean
    order by depth desc
    limit 1
  ) eucl,
  lateral (select (c.r1 * c.n2 * eucl.m2 + c.r2 * c.n1 * eucl.m1) % (c.n1 * c.n2 ) as v) x,
  lateral (select case when x.v < 0 then x.v + c.n1 * c.n2 else x.v end as v) normalized_x
  where c.depth < m.v
)
select r1 as "Answer!!" from chinese_remainder_theorem
order by depth desc
limit 1;

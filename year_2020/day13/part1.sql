with
time as (
  select line::int as t
  from day13
  where line_number=1
),
bus as (
  select u.v::int as interval
  from day13,
  lateral unnest(string_to_array(line, ',')) u(v)
  where line_number=2
  and u.v!='x'
),
wait_time as (
  select bus.interval, bus.interval - (time.t % bus.interval) as time
  from time, bus
)
select interval * time
from wait_time
order by wait_time.time asc
limit 1;

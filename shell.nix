{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell {
  name = "aoc nix shell";

  buildInputs = with pkgs; [ cargo rustc rust-analyzer rustfmt pre-commit ];
}

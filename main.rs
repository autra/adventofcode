use aoc_lib::days;
use aoc_lib::year_2022;
use aoc_lib::year_2023;
use aoc_lib::year_2024;
use clap::Parser;
use colored::Colorize;
use paste::paste;
use std::collections::BTreeMap;
use std::fmt::Display;
use std::fs;

/// program to execute advent of code solution
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// year to run
    year: Option<String>,
    /// day solutions to find
    day: Option<String>,
    /// part of this day to run. If None, part 1 and 2 will be run one after the other
    part: Option<String>,
}

type FnDisplay = fn(&[&str]) -> Box<dyn Display>;
fn run_day(
    year: usize,
    day: usize,
    day_fns: &(Box<FnDisplay>, Box<FnDisplay>),
    part: Option<&str>,
) {
    let day_str = format!("Day {day}:").truecolor(182, 88, 11);
    println!("{day_str}");
    let input = fs::read_to_string(format!("./year_{year}/inputs/day{day}")).unwrap();
    let lines: Vec<&str> = input.lines().collect();
    match part {
        Some("1") => {
            println!("- {}: {}", "part1".italic(), &day_fns.0(&lines));
        }
        Some("2") => {
            println!("- {}: {}", "part2".italic(), &day_fns.1(&lines));
        }
        Some(v) => panic!("Part {v} not implemented"),
        None => {
            println!("- {}: {}", "part1".italic(), &day_fns.0(&lines));
            println!("- {}: {}", "part2".italic(), &day_fns.1(&lines));
        }
    };
}

fn run_year(
    year: usize,
    days_map: &BTreeMap<usize, (Box<FnDisplay>, Box<FnDisplay>)>,
    day: Option<&str>,
    part: Option<&str>,
) {
    let year_str = format!("Year {year}:").blue().bold().underline();
    println!("{}", year_str);
    // year_fn(day.as_deref(), part.as_deref());
    if let Some(day) = day {
        let day = day.parse().expect("day should be an integer");
        if let Some(day_fns) = days_map.get(&day) {
            run_day(year, day, day_fns, part);
        } else {
            panic!("Day {day} not implemented yet!");
        }
    } else {
        // run all days
        for day_fn in days_map {
            run_day(year, *day_fn.0, day_fn.1, part);
        }
    }
}

fn main() {
    let args = Args::parse();

    let years_fn = BTreeMap::from([
        (
            2022,
            days!(2022, (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15)),
        ),
        (2023, days!(2023, (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12))),
        (2024, days!(2024, (1, 2, 3, 4, 5))),
    ]);

    if let Some(year) = args.year {
        let year: usize = year.parse().expect("year argument should be an integer");
        if let Some(days_map) = years_fn.get(&year) {
            run_year(year, &days_map, args.day.as_deref(), args.part.as_deref());
        } else {
            panic!("Year {year} not implemented yet!");
        }
    } else {
        for (year, days_map) in years_fn {
            run_year(year, &days_map, args.day.as_deref(), args.part.as_deref());
            println!("\n");
        }
    }
}
